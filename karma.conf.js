// Karma configuration
// Generated on Fri Aug 25 2017 08:15:01 GMT-0400 (EDT)



module.exports = function (config) {
    config.set({

        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: '',


        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ['mocha', 'karma-typescript'],


        // list of files / patterns to load in the browser
        files: [
            {pattern: "src/**/*.ts"}, // *.tsx for React Jsx
            {pattern: "tests/**/*.spec.ts"} // *.tsx for React Jsx
        ],


        // list of files to exclude
        exclude: ['src/router.ts', 'src/browser_websocket.ts', 'src/index.ts'],


        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {
            "**/*.ts": ["karma-typescript"] // *.tsx for React Jsx
        },


        // test results reporter to use
        // possible values: 'dots', 'progress'
        // available reporters: https://npmjs.org/browse/keyword/karma-reporter
        reporters: ['progress', "karma-typescript"],


        // web server port
        port: 9876,


        // enable / disable colors in the output (reporters and logs)
        colors: true,


        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,


        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: ['ChromeHeadless'],

        plugins: ['karma-chrome-launcher', 'karma-mocha', "karma-typescript"],

        karmaTypescriptConfig: {
            tsconfig: "./tests/tsconfig.json",
            coverageOptions: {
                exclude: [/.*/]
            },
            bundlerOptions: {
                transforms: [
                    function (context, callback) {
                        const test = /\/websocket.ts/;
                        if (test.exec(context.module) !== null) {
                            context.source = 'export const WS = WebSocket;';
                            return callback(undefined, true);
                        }
                        return callback(undefined, false);
                    }
                ]
            }
        },

        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        // Karma seems to have problems with timeouts if the Karma server isn't reset between each run
        // I suspect it has to do with the older version of Socket IO used by Karma, but not sure
        singleRun: true,


        // Concurrency level
        // how many browser should be started simultaneous
        concurrency: Infinity

    })
};
