"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var uws_1 = require("uws");
var glue_event_1 = require("./glue_event");
var log_level_1 = require("./log_level");
var domain_1 = require("./domain");
var util_1 = require("util");
var noop = function () { };
var Router = (function () {
    function Router(options, logLevel) {
        if (logLevel === void 0) { logLevel = log_level_1.LogLevel.NONE; }
        var _this = this;
        this._lobby = [];
        this._domains = {};
        this._onDomainCreated = noop;
        this._onDomainDestroyed = noop;
        this._onServerCreated = noop;
        this._newConnectionHandler = function () { return { success: true }; };
        this._logHandler = function (logLevel, message) {
            var time = new Date().toISOString().substr(11).replace('Z', "");
            var msg = "[" + log_level_1.LogLevel[logLevel] + ": " + time + "]" + ": " + message;
            switch (logLevel) {
                case log_level_1.LogLevel.DEBUG:
                case log_level_1.LogLevel.INFO:
                    console.log(msg);
                    break;
                case log_level_1.LogLevel.WARNING:
                case log_level_1.LogLevel.ERROR:
                    console.error(msg);
                    break;
            }
        };
        this.connectionTimeout = 5000;
        this.logLevel = logLevel;
        options.handleProtocols = function (protocols) {
            for (var i = 0; i < protocols.length; i++) {
                if (protocols[i] === 'glue')
                    return true;
            }
            return false;
        };
        this._server = new uws_1.Server(options, function (err) {
            if (err) {
                throw new Error('Could not create ws server: ' + JSON.stringify(err));
            }
            else {
                _this._onServerCreated();
            }
        });
        this.server.on('error', function (err) {
            throw err;
        });
        this.server.on('connection', function (socket) {
            var headers = socket.upgradeReq.headers;
            _this._lobby.push({ socket: socket, connectionTime: Date.now() });
            if (_this.logLevel <= log_level_1.LogLevel.INFO) {
                var origin = (headers instanceof Array) ? headers[0]['origin'] : headers['origin'];
                _this.log(log_level_1.LogLevel.INFO, "Socket connecting from " + origin);
            }
            socket.onerror = function (error) {
                var connection = _this.getConnection(socket);
                _this.clearConnection(connection);
                _this.log(log_level_1.LogLevel.ERROR, "Router socket error: " + error.toString());
            };
            socket.onmessage = function (message) {
                try {
                    var e = glue_event_1.openEnvelope(message.data);
                    if (e.event.type !== glue_event_1.EventType.CONNECT) {
                        throw new Error("New client sent message other than CONNECT: " + e.event.type);
                    }
                    var event_1 = e.event;
                    _this.onConnect(socket, event_1, headers);
                }
                catch (exc) {
                    socket.close(1011, "Could not connect due to router error.");
                    _this.log(log_level_1.LogLevel.ERROR, "Message: " + JSON.stringify(message.data) + ", EXCEPTION: " + exc.toString());
                }
            };
            socket.onclose = function (event) {
                _this.log(log_level_1.LogLevel.INFO, "Closing socket: code: " + event.code + " reason: " + event.reason);
                var connection = _this.getConnection(socket);
                if (connection) {
                    _this.clearConnection(connection);
                }
            };
        });
        process.on('exit', function () {
            _this.shutDown();
        });
        process.on('SIGINT', function () {
            _this.log(log_level_1.LogLevel.WARNING, 'Router received SIGINT');
            process.exit(2);
        });
        process.on('uncaughtException', function (err) {
            _this.log(log_level_1.LogLevel.ERROR, 'Router had unhandled exception: ' + err.message);
            process.exit(99);
        });
    }
    Object.defineProperty(Router.prototype, "server", {
        get: function () { return this._server; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Router.prototype, "onDomainCreated", {
        set: function (handler) { this._onDomainCreated = handler; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Router.prototype, "onDomainDestroyed", {
        set: function (handler) { this._onDomainDestroyed = handler; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Router.prototype, "onServerCreated", {
        set: function (handler) { this._onServerCreated = handler; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Router.prototype, "onHandleNewConnection", {
        set: function (handler) { this._newConnectionHandler = handler; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Router.prototype, "connectionTimeout", {
        get: function () { return this._connectionTimeout; },
        set: function (interval) {
            var _this = this;
            this._connectionTimeout = interval;
            if (this._connectionTimer) {
                clearInterval(this._connectionTimer);
            }
            this._connectionTimer = setInterval(function () { _this.clearStaleConnections(); }, this._connectionTimeout);
        },
        enumerable: true,
        configurable: true
    });
    ;
    ;
    Router.prototype.onConnect = function (socket, event, headers) {
        var validation = this._newConnectionHandler(event, headers);
        if (util_1.isUndefined(validation) || util_1.isUndefined(validation.success)) {
            throw new Error("onHandleNewConnection has to return an object that has a 'success' property (e.g. {success:false})");
        }
        if (!validation.success) {
            socket.close(4001, "Failed validation: " + validation.reason);
        }
        else {
            var domain = this._domains[event.domain];
            if (!domain) {
                socket.close(4002, "Missing domain: " + event.domain);
            }
            else {
                domain.connectClient(socket, event.id, event.roles);
                var connection = this.getConnection(socket);
                if (connection)
                    this.clearConnection(connection);
            }
        }
    };
    Router.prototype.shutDown = function () {
        var _this = this;
        Object.keys(this._domains).forEach(function (key) {
            var domain = _this._domains[key];
            domain.close();
        });
        this._server.close();
    };
    Router.prototype.getConnection = function (socket) {
        for (var i = 0; i < this._lobby.length; i++) {
            var connection = this._lobby[i];
            if (connection.socket === socket)
                return connection;
        }
        return null;
    };
    Router.prototype.clearConnection = function (connection) {
        for (var i = 0; i < this._lobby.length; i++) {
            if (this._lobby[i] === connection) {
                this._lobby.splice(i, 1);
                break;
            }
        }
    };
    Router.prototype.createDomain = function (domainName, shouldCloseOnEmpty) {
        var domain = this._domains[domainName];
        if (util_1.isUndefined(domain)) {
            this._domains[domainName] = domain = new domain_1.Domain(domainName, this, this.logLevel, shouldCloseOnEmpty);
            this._onDomainCreated(domainName);
            this.log(log_level_1.LogLevel.INFO, "Created domain: " + domainName);
        }
        return domain;
    };
    Router.prototype.closeDomain = function (domainName) {
        var domain = this._domains[domainName];
        if (domain !== undefined) {
            domain.close();
            delete this._domains[domainName];
            this.log(log_level_1.LogLevel.INFO, "Destroyed domain: " + domainName);
            this._onDomainDestroyed(domainName);
        }
    };
    Router.prototype.getActiveDomains = function () {
        return Object.keys(this._domains);
    };
    Router.prototype.getDomain = function (name) {
        var domain = this._domains[name];
        return domain ? domain : null;
    };
    Router.prototype.transferToDomain = function (socket, event) {
        this.onConnect(socket, event, null);
    };
    Router.prototype.clearStaleConnections = function () {
        var now = Date.now();
        for (var i = this._lobby.length - 1; i > 0; i--) {
            var connection = this._lobby[i];
            if ((now - connection.connectionTime) > this._connectionTimeout - 2) {
                connection.socket.close(4004, "Timed out waiting for Connect event");
                this.clearConnection(connection);
            }
        }
    };
    Router.prototype.setLogHandler = function (handler) {
        this._logHandler = handler;
    };
    Router.prototype.log = function (level, message) {
        if (level >= this.logLevel) {
            this._logHandler(level, message);
        }
    };
    Object.defineProperty(Router.prototype, "logLevel", {
        get: function () {
            return this._logLevel;
        },
        set: function (logLevel) {
            this._logLevel = logLevel;
        },
        enumerable: true,
        configurable: true
    });
    return Router;
}());
exports.Router = Router;
//# sourceMappingURL=router.js.map