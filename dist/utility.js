"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var IdUpperBound = (Math.pow(2, 53) - 1);
function createId() {
    var id = 0;
    while (id === 0) {
        id = Math.floor(Math.random() * IdUpperBound);
    }
    return id;
}
exports.createId = createId;
function removeAtIndex(list, index) {
    list.splice(index, 1);
}
exports.removeAtIndex = removeAtIndex;
function removeElement(list, element) {
    if (list === undefined)
        return;
    var i = list.indexOf(element);
    if (i !== -1) {
        list.splice(i, 1);
    }
}
exports.removeElement = removeElement;
function removeFirstMatchingElement(list, pred) {
    if (list === undefined)
        return undefined;
    for (var i = 0; i < list.length; i++) {
        var el = list[i];
        if (pred(el)) {
            list.splice(i, 1);
            return el;
        }
    }
    return undefined;
}
exports.removeFirstMatchingElement = removeFirstMatchingElement;
function forEachValue(obj, fn) {
    var keys = Object.keys(obj);
    var l = keys.length;
    for (var i = 0; i < l; i++) {
        fn(obj[keys[i]]);
    }
}
exports.forEachValue = forEachValue;
//# sourceMappingURL=utility.js.map