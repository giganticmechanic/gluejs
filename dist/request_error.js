"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ErrorType;
(function (ErrorType) {
    ErrorType[ErrorType["REQUEST_TIMEOUT"] = 0] = "REQUEST_TIMEOUT";
    ErrorType[ErrorType["UNHANDLED_REQUEST"] = 1] = "UNHANDLED_REQUEST";
    ErrorType[ErrorType["REQUEST_FAILED"] = 2] = "REQUEST_FAILED";
})(ErrorType = exports.ErrorType || (exports.ErrorType = {}));
var RequestError = (function () {
    function RequestError(type, reason) {
        this.type = type;
        this.reason = reason;
    }
    RequestError.createRequestTimeoutError = function () {
        return new RequestError(ErrorType.REQUEST_TIMEOUT, "Request timed out");
    };
    ;
    RequestError.createRequestFailedError = function () {
        return new RequestError(ErrorType.REQUEST_FAILED, "Request could not be completed due to error at callee");
    };
    ;
    RequestError.createUnhandledRequestError = function (unhandledRequest) {
        return new RequestError(ErrorType.UNHANDLED_REQUEST, "Client has no handler for request: " + unhandledRequest.name);
    };
    ;
    return RequestError;
}());
exports.RequestError = RequestError;
//# sourceMappingURL=request_error.js.map