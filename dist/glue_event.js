"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EventType;
(function (EventType) {
    EventType[EventType["CONNECT"] = 0] = "CONNECT";
    EventType[EventType["WELCOME"] = 1] = "WELCOME";
    EventType[EventType["MESSAGE"] = 2] = "MESSAGE";
    EventType[EventType["REQUEST"] = 3] = "REQUEST";
    EventType[EventType["RESPONSE"] = 4] = "RESPONSE";
    EventType[EventType["HEARTBEAT"] = 5] = "HEARTBEAT";
    EventType[EventType["ROLE_CHANGED"] = 6] = "ROLE_CHANGED";
    EventType[EventType["ROLE_SUBSCRIBE"] = 7] = "ROLE_SUBSCRIBE";
    EventType[EventType["ROLE_UNSUBSCRIBE"] = 8] = "ROLE_UNSUBSCRIBE";
    EventType[EventType["ROLE_SUBSCRIPTION_RESPONSE"] = 9] = "ROLE_SUBSCRIPTION_RESPONSE";
})(EventType = exports.EventType || (exports.EventType = {}));
function createEnvelope(evt, targets) {
    if (targets === void 0) { targets = []; }
    var o = { event: evt, targets: targets };
    return JSON.stringify(o);
}
exports.createEnvelope = createEnvelope;
function openEnvelope(json) {
    var o = JSON.parse(json);
    return { event: o.event, targets: o.targets };
}
exports.openEnvelope = openEnvelope;
var GlueEvent = (function () {
    function GlueEvent() {
    }
    GlueEvent.createConnect = function (domain, id, roles, context) {
        return { type: EventType.CONNECT, domain: domain, id: id, roles: roles, context: context };
    };
    GlueEvent.createMessage = function (name, targetRole, sourceRole, source, data) {
        return { type: EventType.MESSAGE, targetRole: targetRole, source: source, sourceRole: sourceRole, name: name, data: data };
    };
    GlueEvent.createRequest = function (name, targetRole, sourceRole, source, requestId, data) {
        return { type: EventType.REQUEST, name: name, targetRole: targetRole, data: data, sourceRole: sourceRole, source: source, requestId: requestId };
    };
    GlueEvent.createResponse = function (name, targetRole, sourceRole, source, requestId, data, error) {
        return { type: EventType.RESPONSE, name: name, targetRole: targetRole, source: source, sourceRole: sourceRole, data: data, requestId: requestId, error: error };
    };
    GlueEvent.createWelcome = function (domain) {
        return { type: EventType.WELCOME, domain: domain };
    };
    GlueEvent.createHeartbeat = function (timestamp) {
        return { type: EventType.HEARTBEAT, timestamp: timestamp };
    };
    GlueEvent.createRoleChangedEvent = function (role, roleOwnerId, wasCreated) {
        return { type: EventType.ROLE_CHANGED, role: role, roleOwnerId: roleOwnerId, wasCreated: wasCreated };
    };
    GlueEvent.createRoleSubscribeEvent = function (role, clientId) {
        return { type: EventType.ROLE_SUBSCRIBE, role: role, subscriberId: clientId };
    };
    GlueEvent.createRoleUnsubscribeEvent = function (role, clientId) {
        return { type: EventType.ROLE_UNSUBSCRIBE, role: role, subscriberId: clientId };
    };
    GlueEvent.createRoleSubscriptionResponseEvent = function (role, roleOwners) {
        return { type: EventType.ROLE_SUBSCRIPTION_RESPONSE, role: role, roleOwners: roleOwners };
    };
    return GlueEvent;
}());
exports.GlueEvent = GlueEvent;
//# sourceMappingURL=glue_event.js.map