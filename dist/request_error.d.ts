import { RequestEvent } from "./glue_event";
export declare enum ErrorType {
    REQUEST_TIMEOUT = 0,
    UNHANDLED_REQUEST = 1,
    REQUEST_FAILED = 2,
}
export declare class RequestError {
    type: ErrorType;
    reason: string;
    constructor(type: ErrorType, reason: string);
    static createRequestTimeoutError(): RequestError;
    static createRequestFailedError(): RequestError;
    static createUnhandledRequestError(unhandledRequest: RequestEvent): RequestError;
}
