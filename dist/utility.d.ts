export declare function createId(): number;
export declare function removeAtIndex(list: Array<any>, index: number): void;
export declare function removeElement(list: Array<any>, element: any): void;
export declare function removeFirstMatchingElement(list: Array<any>, pred: (el: any) => boolean): any;
export declare function forEachValue(obj: {}, fn: (val: any) => void): void;
