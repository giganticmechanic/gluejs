/// <reference types="uws" />
import { WS } from './websocket';
import { MessageEvent, RequestEvent } from "./glue_event";
import { RequestError } from "./request_error";
export interface ClientParameters {
    url?: string;
    domain: string;
    roles?: Array<Role>;
    context?: any;
}
export interface Logger {
    log: (s: string) => void;
    warn: (s: string) => void;
    error: (s: string) => void;
}
export declare type ResponseHandler = (data: any | null, error: RequestError | null, source: RoleProxy | null) => void;
export interface PendingRequest {
    responseHandler: ResponseHandler;
    start: number;
}
export interface RoleProxy {
    id: number;
    role: string;
}
export declare class Role {
    private _name;
    readonly name: string;
    client: Client | null;
    readonly id: number;
    private _messageHandlers;
    private _requestHandlers;
    private _subscriptions;
    constructor(name: string);
    subscribe(name: string, onConnect?: (rp: RoleProxy) => void, onDisconnect?: (rp: RoleProxy) => void): void;
    unsubscribe(name: string): void;
    isSubscribed(roleName: string): boolean;
    handleConnect(proxy: RoleProxy): void;
    handleDisconnect(proxy: RoleProxy): void;
    getProxy(name: string): RoleProxy | null;
    getProxies(name: string): Array<RoleProxy>;
    sendTo(target: RoleProxy, messageName: string, data?: any): void;
    sendToAll(roleName: string, messageName: string, data?: any): void;
    requestFrom(target: RoleProxy, msgName: string, handler: ResponseHandler): void;
    requestFrom(target: RoleProxy, msgName: string, data: any, handler: ResponseHandler): void;
    onWelcome: (domain: string) => void;
    onClose: (expected: boolean, code: number, reason: string) => void;
    setMessageHandler(messageName: string, handler: (data: any, source: RoleProxy) => void): void;
    setRequestHandler(requestName: string, handler: (data: any, source: RoleProxy) => any): void;
    handleMessage(evt: MessageEvent): boolean;
    handleRequest(evt: RequestEvent): {
        data: any;
        err: RequestError | null;
    };
}
export declare class Client {
    private _url;
    readonly url: string;
    private _id;
    readonly id: number;
    private _targetDomain;
    targetDomain: string;
    private _context;
    context: any;
    private _estimatedLatency;
    readonly estimatedLatency: number;
    private _logger;
    logger: Logger;
    private _updateTimer;
    private _heartbeatTimeout;
    heartbeatTimeout: number;
    private _lastHeartbeatSent;
    private _pendingHeartbeats;
    private _heartbeatInterval;
    heartbeatInterval: number;
    private _maxMissedHeartbeats;
    maxMissedHeartbeats: number;
    private _missedHeartbeats;
    private _updateInterval;
    updateInterval: number;
    private _roles;
    readonly roles: {
        [name: string]: Role;
    };
    private _socket;
    readonly socket: WS;
    private _isConnected;
    readonly isConnected: boolean;
    private _requestTimeout;
    requestTimeout: number;
    private _eventHandlers;
    private _onWelcome;
    onWelcome: (name: string) => void;
    private _onClose;
    onClose: (expected: boolean, code: number, reason: string) => void;
    private _onError;
    onError: (err: any) => void;
    private _onHeartbeatSent;
    onHeartbeatSent: () => void;
    private _onHeartbeatReceived;
    onHeartbeatReceived: () => void;
    constructor(params: ClientParameters, idGenerator?: () => number, logger?: Logger);
    private connect(isReconnecting?);
    private cleanUp();
    private initEventHandlers();
    closeConnection(): void;
    transferDomain(domainName: string): void;
    private update();
    private updateHearbeatTimers(time);
    sendData(data: string): void;
    _pendingRequests: {
        [id: number]: PendingRequest;
    };
    private handleRequest(evt);
    private handleResponse(evt);
    private _updateRequests(time);
    private _roleSubscribers;
    private _proxies;
    cacheProxy(proxy: RoleProxy): void;
    clearProxy(id: number): void;
    isCached(proxy: RoleProxy): boolean;
    _getProxyWithRole(name: string): RoleProxy | null;
    _getProxiesWithRole(name: string): RoleProxy[];
    _subscribe(subscriber: Role, subscription: string): void;
    _unsubscribe(subscriber: Role, subscription: string): void;
    setRole(role: Role): Role;
    getRole(name: string): Role;
    clearRole(roleName: string): void;
    private _isAutoReconnectEnabled;
    isAutoReconnectEnabled: boolean;
    private _isReconnecting;
    private _reconnectTimer;
    private _reconnectDelay;
    reconnectDelay: number;
    private _maxReconnectAttempts;
    maxReconnectAttempts: number;
    private _reconnectAttempts;
    readonly reconnectAttempts: number;
    private _reconnectAttemptHandler;
    onReconnectAttempt: () => void;
    private _reconnectHandler;
    onReconnect: () => void;
    private scheduleReconnect();
    private getBackoff();
    reconnect(): void;
}
