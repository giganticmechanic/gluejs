"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var glue_event_1 = require("./glue_event");
var utility_1 = require("./utility");
var log_level_1 = require("./log_level");
var util_1 = require("util");
var ClientData = (function () {
    function ClientData(socket, id) {
        this._roleSubscriptions = [];
        this._socket = socket;
        this._id = id;
        this._roles = [];
    }
    Object.defineProperty(ClientData.prototype, "id", {
        get: function () { return this._id; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ClientData.prototype, "roles", {
        get: function () { return this._roles; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ClientData.prototype, "socket", {
        get: function () { return this._socket; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ClientData.prototype, "roleSubscriptions", {
        get: function () { return this._roleSubscriptions; },
        enumerable: true,
        configurable: true
    });
    return ClientData;
}());
exports.ClientData = ClientData;
var Domain = (function () {
    function Domain(domainName, parent, logLevel, shouldCloseOnEmpty) {
        if (logLevel === void 0) { logLevel = log_level_1.LogLevel.NONE; }
        var _this = this;
        this._clients = {};
        this.handleSendError = function (err) {
            if (!util_1.isUndefined(err)) {
                _this.log(log_level_1.LogLevel.ERROR, 'Socket send error: ' + JSON.stringify(err));
            }
        };
        this._name = domainName;
        this._parent = parent;
        this._shouldCloseOnEmpty = shouldCloseOnEmpty;
    }
    Object.defineProperty(Domain.prototype, "parent", {
        get: function () { return this._parent; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Domain.prototype, "name", {
        get: function () { return this._name; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Domain.prototype, "shouldCloseOnEmpty", {
        get: function () { return this._shouldCloseOnEmpty; },
        set: function (newValue) { this._shouldCloseOnEmpty = newValue; },
        enumerable: true,
        configurable: true
    });
    Domain.prototype.roleCreated = function (role, clientId) {
        var client = this._clients[clientId];
        if (client !== undefined) {
            client.roles.push(role);
            this.sendRoleChangedEvent(glue_event_1.GlueEvent.createRoleChangedEvent(role, clientId, true));
        }
        else {
            this.log(log_level_1.LogLevel.ERROR, "Unknown client adding role: (client id)" + clientId + ", (role) " + role);
        }
    };
    Domain.prototype.roleDestroyed = function (role, clientId) {
        var client = this._clients[clientId];
        if (client !== undefined) {
            var i = client.roles.indexOf(role);
            if (i != -1) {
                client.roles.splice(i, 1);
                this.sendRoleChangedEvent(glue_event_1.GlueEvent.createRoleChangedEvent(role, clientId, false));
            }
            else {
                this.log(log_level_1.LogLevel.ERROR, "Client removing unknown role: (client id)" + clientId + ", (role) " + role);
            }
        }
        else {
            this.log(log_level_1.LogLevel.ERROR, "Unknown client removing role: (client id)" + clientId + ", (role) " + role);
        }
    };
    Domain.prototype.sendRoleChangedEvent = function (evt) {
        var _this = this;
        var messageString = JSON.stringify(evt);
        utility_1.forEachValue(this._clients, function (client) {
            if (client.id !== evt.roleOwnerId && client.roleSubscriptions.includes(evt.role)) {
                client.socket.send(messageString, _this.handleSendError);
            }
        });
    };
    Domain.prototype.handleRoleChanged = function (source, e) {
        if (e.wasCreated) {
            this.roleCreated(e.role, source.id);
        }
        else {
            this.roleDestroyed(e.role, source.id);
        }
    };
    Domain.prototype.handleRoleSubscribe = function (source, evt) {
        var clientsWithRole = [];
        utility_1.forEachValue(this._clients, function (client) {
            if (client.roles.includes(evt.role) && client.id !== source.id) {
                clientsWithRole.push(client.id);
            }
        });
        var response = glue_event_1.GlueEvent.createRoleSubscriptionResponseEvent(evt.role, clientsWithRole);
        source.roleSubscriptions.push(evt.role);
        source.socket.send(JSON.stringify(response), this.handleSendError);
    };
    Domain.prototype.handleRoleUnsubscribe = function (source, e) {
        utility_1.removeElement(source.roleSubscriptions, e.role);
    };
    Domain.prototype.log = function (logLevel, message) {
        this.parent.log(logLevel, message);
    };
    Domain.prototype.handleEvent = function (event, source, targets) {
        try {
            switch (event.type) {
                case glue_event_1.EventType.CONNECT:
                    this.transferDomain(source, event);
                    break;
                case glue_event_1.EventType.MESSAGE:
                    this.handleMessage(source, event, targets);
                    break;
                case glue_event_1.EventType.REQUEST:
                    this.handleRequest(this._clients[targets[0]], event);
                    break;
                case glue_event_1.EventType.RESPONSE:
                    this.handleResponse(this._clients[targets[0]], event);
                    break;
                case glue_event_1.EventType.HEARTBEAT:
                    this.handleHeartbeat(source, event);
                    break;
                case glue_event_1.EventType.ROLE_CHANGED:
                    this.handleRoleChanged(source, event);
                    break;
                case glue_event_1.EventType.ROLE_SUBSCRIBE:
                    this.handleRoleSubscribe(source, event);
                    break;
                case glue_event_1.EventType.ROLE_UNSUBSCRIBE:
                    this.handleRoleUnsubscribe(source, event);
                    break;
                default:
                    throw new Error('Domains cannot handle event type: ' + event.type);
            }
        }
        catch (e) {
            this.log(log_level_1.LogLevel.ERROR, "Exception handling event: " + JSON.stringify(event) + "\nException:\n" + e);
        }
    };
    ;
    Domain.prototype.handleMessage = function (source, event, targets) {
        var message = JSON.stringify(event);
        for (var index in targets) {
            var targetId = targets[index];
            if (targetId !== source.id) {
                var client = this._clients[targetId];
                if (client) {
                    client.socket.send(message, this.handleSendError);
                }
            }
        }
        if (log_level_1.LogLevel.DEBUG >= this._parent.logLevel) {
            this.log(log_level_1.LogLevel.DEBUG, "Forwarding message: " + JSON.stringify(event) + " to targets: " + JSON.stringify(targets));
        }
    };
    Domain.prototype.handleRequest = function (target, event) {
        if (target === undefined)
            throw new Error("Nonexistent target for request:" + JSON.stringify(event));
        target.socket.send(JSON.stringify(event), this.handleSendError);
        if (log_level_1.LogLevel.DEBUG >= this._parent.logLevel) {
            this.log(log_level_1.LogLevel.DEBUG, "Forwarding request to " + target.id + ": " + JSON.stringify(event));
        }
    };
    Domain.prototype.handleResponse = function (target, event) {
        if (target === undefined)
            throw new Error("Nonexistent target for response:" + JSON.stringify(event));
        target.socket.send(JSON.stringify(event), this.handleSendError);
        if (log_level_1.LogLevel.DEBUG >= this._parent.logLevel) {
            this.log(log_level_1.LogLevel.DEBUG, "Forwarding response to " + target.id + ": " + JSON.stringify(event));
        }
    };
    Domain.prototype.handleHeartbeat = function (source, event) {
        source.socket.send(JSON.stringify(event), this.handleSendError);
        if (log_level_1.LogLevel.DEBUG >= this._parent.logLevel) {
            this.log(log_level_1.LogLevel.DEBUG, "Heartbeat from: " + source.id);
        }
    };
    Domain.prototype.handleClose = function (socket) {
        var _this = this;
        utility_1.forEachValue(this._clients, function (client) {
            if (client.socket === socket) {
                for (var i = client.roles.length - 1; i >= 0; i--) {
                    _this.roleDestroyed(client.roles[i], client.id);
                }
                delete (_this._clients[client.id]);
                return;
            }
        });
    };
    ;
    Domain.prototype.onError = function (client, error) {
        this.log(log_level_1.LogLevel.ERROR, 'Socket Error (client id: ' + client.id + '): ' + JSON.stringify(error));
    };
    ;
    Domain.prototype.onMessage = function (source, message) {
        try {
            var e = glue_event_1.openEnvelope(message.data);
            this.handleEvent(e.event, source, e.targets);
        }
        catch (exception) {
            this.log(log_level_1.LogLevel.ERROR, 'Exception at socket.onmessage: (MSG: ' + JSON.stringify(message) + '), (EXC: ' + exception.toString() + ')');
        }
    };
    ;
    Domain.prototype.onClose = function (client) {
        try {
            this.handleClose(client.socket);
            this.log(log_level_1.LogLevel.INFO, "Client disconnected from " + this.name + ": " + client.id);
            if (this._shouldCloseOnEmpty && Object.keys(this._clients).length === 0) {
                this.parent.closeDomain(this.name);
            }
        }
        catch (exception) {
            this.log(log_level_1.LogLevel.ERROR, 'Exception at onClose: ' + exception.toString());
        }
    };
    ;
    Domain.prototype.connectClient = function (socket, id, roles) {
        var _this = this;
        if (this._clients[id] !== undefined) {
            socket.close(4003, "Connecting with duplicate id");
            this.log(log_level_1.LogLevel.WARNING, "Client attempting to connect with duplicate id: " + id);
            return;
        }
        var client = new ClientData(socket, id);
        this._clients[client.id] = client;
        roles.forEach(function (role) {
            _this.roleCreated(role, client.id);
        });
        client.socket.onmessage = function (message) {
            _this.onMessage(client, message);
        };
        client.socket.onerror = function (err) {
            _this.onError(client, err);
        };
        client.socket.onclose = function () {
            _this.onClose(client);
        };
        var welcomeEvent = glue_event_1.GlueEvent.createWelcome(this.name);
        client.socket.send(JSON.stringify(welcomeEvent), this.handleSendError);
        if (log_level_1.LogLevel.INFO > this._parent.logLevel) {
            this.log(log_level_1.LogLevel.INFO, "Client connected to " + this.name + ". Roles: " + JSON.stringify(client.roles) + ", ID: " + client.id);
        }
    };
    Domain.prototype.close = function () {
        for (var id in this._clients) {
            var client = this._clients[id];
            client.socket.close(1013, 'Try again later');
        }
    };
    Domain.prototype.killClient = function (clientId) {
        var client = this._clients[clientId];
        if (client) {
            client.socket.close(1013, 'Try again later');
        }
    };
    Domain.prototype.hasClient = function (id) {
        return this._clients[id] !== undefined;
    };
    Domain.prototype.transferDomain = function (client, event) {
        if (event.domain !== this.name) {
            this._parent.transferToDomain(client.socket, event);
            this.onClose(client);
        }
    };
    return Domain;
}());
exports.Domain = Domain;
//# sourceMappingURL=domain.js.map