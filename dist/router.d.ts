/// <reference types="uws" />
/// <reference types="node" />
import { Server } from 'uws';
import * as WebSocket from 'uws';
import { ConnectEvent } from './glue_event';
import { LogLevel } from "./log_level";
import { Domain } from "./domain";
import { IncomingHttpHeaders } from "http";
export declare class Router {
    protected _server: Server;
    readonly server: Server;
    private _lobby;
    private _domains;
    private _onDomainCreated;
    onDomainCreated: (domainName: string) => void;
    private _onDomainDestroyed;
    onDomainDestroyed: (domainName: string) => void;
    private _onServerCreated;
    onServerCreated: () => void;
    private _newConnectionHandler;
    onHandleNewConnection: (evt: ConnectEvent, headers: IncomingHttpHeaders | null) => {
        success: boolean;
        reason?: string;
    };
    private _connectionTimeout;
    connectionTimeout: number;
    private _connectionTimer;
    constructor(options: WebSocket.IServerOptions, logLevel?: LogLevel);
    private onConnect(socket, event, headers);
    shutDown(): void;
    private getConnection(socket);
    private clearConnection(connection);
    createDomain(domainName: string, shouldCloseOnEmpty: boolean): Domain;
    closeDomain(domainName: string): void;
    getActiveDomains(): Array<string>;
    getDomain(name: string): Domain | null;
    transferToDomain(socket: WebSocket, event: ConnectEvent): void;
    private clearStaleConnections();
    private _logHandler;
    setLogHandler(handler: (logLevel: LogLevel, message: string) => void): void;
    log(level: LogLevel, message: string): void;
    private _logLevel;
    logLevel: LogLevel;
}
