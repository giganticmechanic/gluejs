"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var websocket_1 = require("./websocket");
var glue_event_1 = require("./glue_event");
var utility_1 = require("./utility");
var request_error_1 = require("./request_error");
var util_1 = require("util");
var noop = function () { };
var reconnectionCloseCodes = [
    1001,
    1005,
    1006,
    1009,
    1011,
    1012,
    1013,
    1014,
    4002,
    4003,
    4999,
];
var ReadyState;
(function (ReadyState) {
    ReadyState[ReadyState["CONNECTING"] = 0] = "CONNECTING";
    ReadyState[ReadyState["OPEN"] = 1] = "OPEN";
    ReadyState[ReadyState["CLOSING"] = 2] = "CLOSING";
    ReadyState[ReadyState["CLOSED"] = 3] = "CLOSED";
})(ReadyState || (ReadyState = {}));
var Role = (function () {
    function Role(name) {
        this.client = null;
        this._messageHandlers = {};
        this._requestHandlers = {};
        this._subscriptions = {};
        this.onWelcome = noop;
        this.onClose = noop;
        this._name = name;
    }
    Object.defineProperty(Role.prototype, "name", {
        get: function () { return this._name; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Role.prototype, "id", {
        get: function () { return this.client ? this.client.id : 0; },
        enumerable: true,
        configurable: true
    });
    Role.prototype.subscribe = function (name, onConnect, onDisconnect) {
        if (onConnect === void 0) { onConnect = noop; }
        if (onDisconnect === void 0) { onDisconnect = noop; }
        if (!this.client || !this.client.isConnected) {
            throw new Error("Roles can only subscribe when the client is connected (try subscribing in the `onWelcome` handler)");
        }
        onConnect = onConnect || noop;
        onDisconnect = onDisconnect || noop;
        this._subscriptions[name] = { onConnect: onConnect, onDisconnect: onDisconnect };
        this.client._subscribe(this, name);
    };
    Role.prototype.unsubscribe = function (name) {
        delete this._subscriptions[name];
        if (this.client) {
            this.client._unsubscribe(this, name);
        }
    };
    Role.prototype.isSubscribed = function (roleName) {
        return this._subscriptions[roleName] !== undefined;
    };
    Role.prototype.handleConnect = function (proxy) {
        var handlers = this._subscriptions[proxy.role];
        if (handlers)
            handlers.onConnect(proxy);
    };
    Role.prototype.handleDisconnect = function (proxy) {
        var handlers = this._subscriptions[proxy.role];
        if (handlers)
            handlers.onDisconnect(proxy);
    };
    Role.prototype.getProxy = function (name) {
        if (this.client) {
            return this.client._getProxyWithRole(name);
        }
        return null;
    };
    Role.prototype.getProxies = function (name) {
        if (this.client) {
            return this.client._getProxiesWithRole(name);
        }
        return [];
    };
    Role.prototype.sendTo = function (target, messageName, data) {
        if (!this.client) {
            throw new Error("Attempting to send from role that is not attached to client");
        }
        if (!this.client.isConnected) {
            throw new Error("Attempting to send from role with client that is not connected");
        }
        var e = glue_event_1.GlueEvent.createMessage(messageName, target.role, this.name, this.id, data);
        var envelope = glue_event_1.createEnvelope(e, [target.id]);
        this.client.sendData(envelope);
    };
    Role.prototype.sendToAll = function (roleName, messageName, data) {
        if (!this.client) {
            throw new Error("Attempting to send from role that is not attached to client");
        }
        if (!this.client.isConnected) {
            throw new Error("Attempting to send from role with client that is not connected");
        }
        var ids = this.client._getProxiesWithRole(roleName).map(function (p) { return p.id; });
        if (ids) {
            this.client.sendData(glue_event_1.createEnvelope(glue_event_1.GlueEvent.createMessage(messageName, roleName, this.name, this.id, data), ids));
        }
    };
    Role.prototype.requestFrom = function (target, msgName, data, handler) {
        if (!this.client) {
            throw new Error("Attempting to send from role that is not attached to client");
        }
        if (!this.client.isConnected) {
            throw new Error("Attempting to send from role with client that is not connected");
        }
        if (util_1.isUndefined(handler)) {
            if (util_1.isUndefined(data) || !util_1.isFunction(data)) {
                throw new Error("Requests need a response handler");
            }
            else {
                handler = data;
                data = null;
            }
        }
        var requestEvent;
        var requestId = utility_1.createId();
        requestEvent = glue_event_1.GlueEvent.createRequest(msgName, target.role, this.name, this.client.id, requestId, data);
        this.client._pendingRequests[requestId] = { responseHandler: handler, start: Date.now() };
        this.client.sendData(glue_event_1.createEnvelope(requestEvent, [target.id]));
    };
    Role.prototype.setMessageHandler = function (messageName, handler) {
        this._messageHandlers[messageName] = handler;
    };
    Role.prototype.setRequestHandler = function (requestName, handler) {
        this._requestHandlers[requestName] = handler;
    };
    Role.prototype.handleMessage = function (evt) {
        var handler = this._messageHandlers[evt.name];
        if (handler) {
            handler(evt.data, { id: evt.source, role: evt.sourceRole });
            return true;
        }
        else {
            return false;
        }
    };
    Role.prototype.handleRequest = function (evt) {
        var handler = this._requestHandlers[evt.name];
        var err = null;
        var data = undefined;
        if (handler) {
            try {
                data = handler(evt['data'], { id: evt.source, role: evt.sourceRole });
            }
            catch (ex) {
                err = request_error_1.RequestError.createRequestFailedError();
            }
        }
        else {
            err = request_error_1.RequestError.createUnhandledRequestError(evt);
        }
        return { data: data, err: err };
    };
    return Role;
}());
exports.Role = Role;
var Client = (function () {
    function Client(params, idGenerator, logger) {
        if (idGenerator === void 0) { idGenerator = function () { return utility_1.createId(); }; }
        if (logger === void 0) { logger = console; }
        var _this = this;
        this._estimatedLatency = -1;
        this._heartbeatTimeout = 10000;
        this._lastHeartbeatSent = Date.now();
        this._pendingHeartbeats = [];
        this._heartbeatInterval = 15000;
        this._maxMissedHeartbeats = 3;
        this._missedHeartbeats = 0;
        this._updateInterval = 100;
        this._roles = {};
        this._isConnected = false;
        this._requestTimeout = 1000;
        this._eventHandlers = {};
        this._onWelcome = noop;
        this._onClose = noop;
        this._onError = noop;
        this._onHeartbeatSent = noop;
        this._onHeartbeatReceived = noop;
        this._pendingRequests = {};
        this._roleSubscribers = {};
        this._proxies = [];
        this._isAutoReconnectEnabled = true;
        this._isReconnecting = false;
        this._reconnectDelay = 1000;
        this._maxReconnectAttempts = 10;
        this._reconnectAttempts = 0;
        this._reconnectAttemptHandler = noop;
        this._reconnectHandler = noop;
        this.logger = logger;
        if (params.url === undefined) {
            if (window !== undefined) {
                params.url = window.location.origin;
            }
            else {
                throw new Error('Non-browser clients must provide a url parameter');
            }
        }
        this._url = params.url;
        this._targetDomain = params.domain;
        this._roles = {};
        if (params.roles)
            params.roles.forEach(function (role) { _this.setRole(role); });
        this._id = idGenerator();
        this._context = params.context || null;
        this.initEventHandlers();
        try {
            this.connect(false);
        }
        catch (e) {
            this.logger.error("Client could not connect: " + e.message);
        }
    }
    Object.defineProperty(Client.prototype, "url", {
        get: function () { return this._url; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "id", {
        get: function () { return this._id; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "targetDomain", {
        get: function () { return this._targetDomain; },
        set: function (newName) { this._targetDomain = newName; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "context", {
        set: function (context) { this._context = context; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "estimatedLatency", {
        get: function () { return this._estimatedLatency; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "logger", {
        get: function () { return this._logger; },
        set: function (l) { this._logger = l; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "heartbeatTimeout", {
        get: function () { return this._heartbeatTimeout; },
        set: function (interval) { this._heartbeatTimeout = interval; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "heartbeatInterval", {
        get: function () { return this._heartbeatInterval; },
        set: function (newInterval) {
            this._heartbeatInterval = newInterval;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "maxMissedHeartbeats", {
        get: function () { return this._maxMissedHeartbeats; },
        set: function (newValue) { this._maxMissedHeartbeats = newValue; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "updateInterval", {
        get: function () { return this._updateInterval; },
        set: function (interval) { this._updateInterval = Math.max(interval, 0); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "roles", {
        get: function () { return this._roles; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "socket", {
        get: function () { return this._socket; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "isConnected", {
        get: function () { return this._isConnected; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "requestTimeout", {
        get: function () { return this._requestTimeout; },
        set: function (interval) { this._requestTimeout = interval; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "onWelcome", {
        set: function (action) { this._onWelcome = action; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "onClose", {
        set: function (handler) {
            this._onClose = handler;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "onError", {
        set: function (handler) {
            this._onError = handler;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "onHeartbeatSent", {
        set: function (handler) { this._onHeartbeatSent = handler; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "onHeartbeatReceived", {
        set: function (handler) { this._onHeartbeatReceived = handler; },
        enumerable: true,
        configurable: true
    });
    Client.prototype.connect = function (isReconnecting) {
        var _this = this;
        if (isReconnecting === void 0) { isReconnecting = false; }
        this._isReconnecting = isReconnecting;
        this._socket = new websocket_1.WS(this._url, 'glue');
        if (this._isReconnecting) {
            this._reconnectAttempts++;
            this._reconnectAttemptHandler();
        }
        this._socket.onopen = function () {
            _this._reconnectAttempts = 0;
            _this.socket.send(glue_event_1.createEnvelope(glue_event_1.GlueEvent.createConnect(_this.targetDomain, _this.id, Object.keys(_this.roles), _this._context)));
            _this._updateTimer = setInterval(function () { _this.update(); }, _this._updateInterval);
            if (_this._isReconnecting) {
                _this._reconnectHandler();
                _this._isReconnecting = false;
            }
        };
        this._socket.onmessage = function (m) {
            var e = JSON.parse(m.data);
            var handler = _this._eventHandlers[e.type];
            if (handler !== undefined) {
                handler(e);
            }
            else {
                _this.logger.warn('Unhandled message: ' + JSON.stringify(m));
            }
        };
        this._socket.onerror = function (err) {
            _this._onError(err);
        };
        this._socket.onclose = function (event) {
            _this._isConnected = false;
            var wasExpected = event.code === 1000;
            _this._onClose(wasExpected, event.code, event.reason);
            utility_1.forEachValue(_this.roles, function (role) {
                role.onClose(wasExpected, event.code, event.reason);
                _this._proxies.forEach(function (p) {
                    if (role.isSubscribed(p.role)) {
                        role.handleDisconnect(p);
                    }
                });
            });
            _this.cleanUp();
            if (_this._isAutoReconnectEnabled &&
                _this.reconnectAttempts < _this.maxReconnectAttempts &&
                reconnectionCloseCodes.indexOf(event.code) !== -1) {
                _this.scheduleReconnect();
            }
        };
    };
    Client.prototype.cleanUp = function () {
        clearInterval(this._updateTimer);
        this._pendingRequests = {};
        this._roleSubscribers = {};
        this._pendingHeartbeats = [];
        if (this._reconnectTimer)
            clearTimeout(this._reconnectTimer);
    };
    Client.prototype.initEventHandlers = function () {
        var _this = this;
        this._eventHandlers[glue_event_1.EventType.WELCOME] = function (evt) {
            _this._isConnected = true;
            _this._onWelcome(evt.domain);
            var roleNames = Object.keys(_this.roles);
            roleNames.forEach(function (name) {
                var role = _this.roles[name];
                role.onWelcome(evt.domain);
            });
        };
        this._eventHandlers[glue_event_1.EventType.MESSAGE] = function (evt) {
            var role = _this._roles[evt.targetRole];
            if (role) {
                if (!role.handleMessage(evt)) {
                    _this._logger.log(role.name + " cannot handle message: " + JSON.stringify(evt));
                }
            }
            else {
                _this._logger.log("Client needs role named " + evt.targetRole + " to handle message: " + JSON.stringify(evt));
            }
        };
        this._eventHandlers[glue_event_1.EventType.REQUEST] = function (evt) {
            _this.handleRequest(evt);
        };
        this._eventHandlers[glue_event_1.EventType.RESPONSE] = function (evt) {
            _this.handleResponse(evt);
        };
        this._eventHandlers[glue_event_1.EventType.HEARTBEAT] = function (evt) {
            _this._estimatedLatency = (Date.now() - evt.timestamp) / 2;
            var i = _this._pendingHeartbeats.indexOf(evt.timestamp);
            if (i !== -1) {
                utility_1.removeAtIndex(_this._pendingHeartbeats, i);
            }
            _this._missedHeartbeats = 0;
            _this._onHeartbeatReceived();
        };
        this._eventHandlers[glue_event_1.EventType.ROLE_CHANGED] = function (evt) {
            var subscribers = _this._roleSubscribers[evt.role];
            if (subscribers !== undefined) {
                if (evt.wasCreated) {
                    var proxy_1 = { id: evt.roleOwnerId, role: evt.role };
                    _this.cacheProxy(proxy_1);
                    subscribers.forEach(function (subscriber) { subscriber.handleConnect(proxy_1); });
                }
                else {
                    var proxy_2 = { id: evt.roleOwnerId, role: evt.role };
                    subscribers.forEach(function (subscriber) { subscriber.handleDisconnect(proxy_2); });
                    _this.clearProxy(evt.roleOwnerId);
                }
            }
            else {
                _this.logger.warn("Received ROLE_CHANGED event for role that has no subscriber: " + evt.role);
            }
        };
        this._eventHandlers[glue_event_1.EventType.ROLE_SUBSCRIPTION_RESPONSE] = function (evt) {
            var subscribers = _this._roleSubscribers[evt.role];
            if (subscribers) {
                var _loop_1 = function (i) {
                    var id = evt.roleOwners[i];
                    var proxy = { id: id, role: evt.role };
                    _this.cacheProxy(proxy);
                    subscribers.forEach(function (subscriber) { subscriber.handleConnect(proxy); });
                };
                for (var i = 0; i < evt.roleOwners.length; i++) {
                    _loop_1(i);
                }
            }
            else {
                _this.logger.warn("Received ROLE_SUBSCRIPITION_RESPONSE for role that client is not subcribed to: " + evt.role);
            }
        };
    };
    Client.prototype.closeConnection = function () {
        this._isReconnecting = false;
        if (this._reconnectTimer)
            clearTimeout(this._reconnectTimer);
        this._isConnected = false;
        var s = this.socket;
        if (s.readyState === ReadyState.OPEN || s.readyState === ReadyState.CONNECTING)
            this.socket.close(1000, 'Client closed');
    };
    Client.prototype.transferDomain = function (domainName) {
        if (!this._isConnected) {
            throw new Error("Cannot transfer domain if not connected");
        }
        this.cleanUp();
        this._targetDomain = domainName;
        var evt = glue_event_1.GlueEvent.createConnect(this.targetDomain, this.id, Object.keys(this.roles), this._context);
        this.sendData(glue_event_1.createEnvelope(evt));
        this._isConnected = false;
    };
    Client.prototype.update = function () {
        var now = Date.now();
        this.updateHearbeatTimers(now);
        this._updateRequests(now);
    };
    Client.prototype.updateHearbeatTimers = function (time) {
        while (this._pendingHeartbeats.length > 0) {
            if (time - this._pendingHeartbeats[0] > this.heartbeatTimeout) {
                this._missedHeartbeats++;
                this._pendingHeartbeats.splice(0, 1);
                if (this._missedHeartbeats >= this._maxMissedHeartbeats) {
                    this.socket.close(1006, 'Heartbeat missed');
                    return;
                }
            }
            else {
                break;
            }
        }
        if (time - this._lastHeartbeatSent >= this._heartbeatInterval && this.socket.readyState === ReadyState.OPEN) {
            this.sendData(glue_event_1.createEnvelope(glue_event_1.GlueEvent.createHeartbeat(time)));
            this._pendingHeartbeats.push(time);
            this._lastHeartbeatSent = time;
            this._onHeartbeatSent();
        }
    };
    Client.prototype.sendData = function (data) {
        switch (this.socket.readyState) {
            case ReadyState.OPEN:
                this.socket.send(data);
                break;
            case ReadyState.CONNECTING:
                this.logger.error('Attempting to send data through connecting socket: ' + data);
                break;
            case ReadyState.CLOSING:
                this.logger.error('Attempting to send data through closing socket: ' + data);
                break;
            case ReadyState.CLOSED:
                this.logger.error('Attempting to send data through closed socket: ' + data);
                break;
            default:
                break;
        }
    };
    Client.prototype.handleRequest = function (evt) {
        var role = this.roles[evt.targetRole];
        if (role) {
            var result = role.handleRequest(evt);
            var responseEvent = glue_event_1.GlueEvent.createResponse(evt.name, role.name, evt.targetRole, this.id, evt.requestId, result.data, result.err);
            this.sendData(glue_event_1.createEnvelope(responseEvent, [evt.source]));
        }
        else {
            this._logger.log("Client needs role named " + evt.targetRole + " to handle request: " + JSON.stringify(evt));
        }
    };
    Client.prototype.handleResponse = function (evt) {
        var request = this._pendingRequests[evt.requestId];
        if (request) {
            request.responseHandler(evt['data'], evt['error'], { id: evt.source, role: evt.sourceRole });
            delete this._pendingRequests[evt.requestId];
        }
        else {
            this.logger.error('No request for response: ' + JSON.stringify(evt));
        }
    };
    Client.prototype._updateRequests = function (time) {
        for (var pi in this._pendingRequests) {
            var pr = this._pendingRequests[pi];
            if (time - pr.start > this._requestTimeout) {
                pr.responseHandler(null, request_error_1.RequestError.createRequestTimeoutError(), null);
                delete this._pendingRequests[pi];
            }
        }
    };
    Client.prototype.cacheProxy = function (proxy) {
        if (!this.isCached(proxy)) {
            this._proxies.push(proxy);
        }
    };
    Client.prototype.clearProxy = function (id) {
        var i = -1;
        this._proxies.find(function (other, index) {
            if (id === other.id) {
                i = index;
                return true;
            }
            return false;
        });
        if (i >= 0) {
            this._proxies.splice(i, 1);
        }
    };
    Client.prototype.isCached = function (proxy) {
        var found = false;
        this._proxies.find(function (other) {
            if (proxy.id === other.id && proxy.role === other.role) {
                found = true;
                return true;
            }
            return false;
        });
        return found;
    };
    Client.prototype._getProxyWithRole = function (name) {
        var proxies = this._getProxiesWithRole(name);
        return proxies ? proxies[0] : null;
    };
    Client.prototype._getProxiesWithRole = function (name) {
        return this._proxies.filter(function (p) { return p.role === name; });
    };
    Client.prototype._subscribe = function (subscriber, subscription) {
        if (!this._roleSubscribers[subscription]) {
            this._roleSubscribers[subscription] = [];
            this.sendData(glue_event_1.createEnvelope(glue_event_1.GlueEvent.createRoleSubscribeEvent(subscription, this.id)));
        }
        this._roleSubscribers[subscription].push(subscriber);
    };
    Client.prototype._unsubscribe = function (subscriber, subscription) {
        var subscribers = this._roleSubscribers[subscription];
        if (subscribers) {
            var i = subscribers.indexOf(subscriber);
            if (i !== -1) {
                subscribers.splice(i, 1);
            }
            if (subscribers.length === 0) {
                this.sendData(glue_event_1.createEnvelope(glue_event_1.GlueEvent.createRoleUnsubscribeEvent(subscription, this.id)));
            }
        }
        else {
            this._logger.warn("Role " + subscriber.name + " is unsubscribing from " + subscription + ", but never subscribed in the first place");
        }
    };
    Client.prototype.setRole = function (role) {
        var oldRole = this._roles[role.name];
        if (!util_1.isUndefined(oldRole)) {
            oldRole.client = null;
            if (this.isConnected)
                this.sendData(glue_event_1.createEnvelope(glue_event_1.GlueEvent.createRoleChangedEvent(role.name, this._id, false)));
        }
        if (this.isConnected) {
            this.sendData(glue_event_1.createEnvelope(glue_event_1.GlueEvent.createRoleChangedEvent(role.name, this._id, true)));
        }
        this.roles[role.name] = role;
        role.client = this;
        return role;
    };
    Client.prototype.getRole = function (name) {
        return this.roles[name] || null;
    };
    Client.prototype.clearRole = function (roleName) {
        if (this.roles[roleName] !== undefined) {
            delete (this.roles[roleName]);
            if (this.isConnected) {
                this.sendData(glue_event_1.createEnvelope(glue_event_1.GlueEvent.createRoleChangedEvent(roleName, this._id, false)));
            }
        }
    };
    Object.defineProperty(Client.prototype, "isAutoReconnectEnabled", {
        get: function () { return this._isAutoReconnectEnabled; },
        set: function (val) { this._isAutoReconnectEnabled = val; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "reconnectDelay", {
        get: function () { return this._reconnectDelay; },
        set: function (delay) { this._reconnectDelay = delay; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "maxReconnectAttempts", {
        get: function () { return this._maxReconnectAttempts; },
        set: function (attempts) { this._maxReconnectAttempts = attempts; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "reconnectAttempts", {
        get: function () { return this._reconnectAttempts; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "onReconnectAttempt", {
        set: function (handler) { this._reconnectAttemptHandler = handler; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "onReconnect", {
        set: function (handler) { this._reconnectHandler = handler; },
        enumerable: true,
        configurable: true
    });
    Client.prototype.scheduleReconnect = function () {
        var _this = this;
        this._reconnectTimer = setTimeout(function () { _this.connect(true); }, this.getBackoff());
    };
    Client.prototype.getBackoff = function () {
        return this._reconnectDelay * Math.pow(1.5, this._reconnectAttempts) + (this.reconnectDelay * Math.random());
    };
    Client.prototype.reconnect = function () {
        this.closeConnection();
        this.connect(true);
    };
    return Client;
}());
exports.Client = Client;
//# sourceMappingURL=client.js.map