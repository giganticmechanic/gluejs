/// <reference types="uws" />
import * as WebSocket from 'uws';
import { LogLevel } from "./log_level";
import { Router } from "./router";
export declare class ClientData {
    private _id;
    readonly id: number;
    private _roles;
    readonly roles: string[];
    private _socket;
    readonly socket: WebSocket;
    private _roleSubscriptions;
    readonly roleSubscriptions: string[];
    constructor(socket: WebSocket, id: number);
}
export declare class Domain {
    private _clients;
    private _parent;
    readonly parent: Router;
    private _name;
    readonly name: string;
    private _shouldCloseOnEmpty;
    shouldCloseOnEmpty: boolean;
    constructor(domainName: string, parent: Router, logLevel: LogLevel | undefined, shouldCloseOnEmpty: boolean);
    private roleCreated(role, clientId);
    private roleDestroyed(role, clientId);
    private sendRoleChangedEvent(evt);
    private handleRoleChanged(source, e);
    private handleRoleSubscribe(source, evt);
    private handleRoleUnsubscribe(source, e);
    private log(logLevel, message);
    private handleEvent(event, source, targets);
    private handleMessage(source, event, targets);
    private handleRequest(target, event);
    private handleResponse(target, event);
    private handleHeartbeat(source, event);
    private handleSendError;
    private handleClose(socket);
    private onError(client, error);
    private onMessage(source, message);
    private onClose(client);
    connectClient(socket: WebSocket, id: number, roles: Array<string>): void;
    close(): void;
    killClient(clientId: number): void;
    hasClient(id: number): boolean;
    private transferDomain(client, event);
}
