(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define("glue", [], factory);
	else if(typeof exports === 'object')
		exports["glue"] = factory();
	else
		root["glue"] = factory();
})(typeof self !== 'undefined' ? self : this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var websocket_1 = __webpack_require__(1);
var glue_event_1 = __webpack_require__(2);
var utility_1 = __webpack_require__(3);
var request_error_1 = __webpack_require__(4);
var util_1 = __webpack_require__(5);
var noop = function () { };
var reconnectionCloseCodes = [
    1001,
    1005,
    1006,
    1009,
    1011,
    1012,
    1013,
    1014,
    4002,
    4003,
    4999,
];
var ReadyState;
(function (ReadyState) {
    ReadyState[ReadyState["CONNECTING"] = 0] = "CONNECTING";
    ReadyState[ReadyState["OPEN"] = 1] = "OPEN";
    ReadyState[ReadyState["CLOSING"] = 2] = "CLOSING";
    ReadyState[ReadyState["CLOSED"] = 3] = "CLOSED";
})(ReadyState || (ReadyState = {}));
var Role = (function () {
    function Role(name) {
        this.client = null;
        this._messageHandlers = {};
        this._requestHandlers = {};
        this._subscriptions = {};
        this.onWelcome = noop;
        this.onClose = noop;
        this._name = name;
    }
    Object.defineProperty(Role.prototype, "name", {
        get: function () { return this._name; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Role.prototype, "id", {
        get: function () { return this.client ? this.client.id : 0; },
        enumerable: true,
        configurable: true
    });
    Role.prototype.subscribe = function (name, onConnect, onDisconnect) {
        if (onConnect === void 0) { onConnect = noop; }
        if (onDisconnect === void 0) { onDisconnect = noop; }
        if (!this.client || !this.client.isConnected) {
            throw new Error("Roles can only subscribe when the client is connected (try subscribing in the `onWelcome` handler)");
        }
        onConnect = onConnect || noop;
        onDisconnect = onDisconnect || noop;
        this._subscriptions[name] = { onConnect: onConnect, onDisconnect: onDisconnect };
        this.client._subscribe(this, name);
    };
    Role.prototype.unsubscribe = function (name) {
        delete this._subscriptions[name];
        if (this.client) {
            this.client._unsubscribe(this, name);
        }
    };
    Role.prototype.isSubscribed = function (roleName) {
        return this._subscriptions[roleName] !== undefined;
    };
    Role.prototype.handleConnect = function (proxy) {
        var handlers = this._subscriptions[proxy.role];
        if (handlers)
            handlers.onConnect(proxy);
    };
    Role.prototype.handleDisconnect = function (proxy) {
        var handlers = this._subscriptions[proxy.role];
        if (handlers)
            handlers.onDisconnect(proxy);
    };
    Role.prototype.getProxy = function (name) {
        if (this.client) {
            return this.client._getProxyWithRole(name);
        }
        return null;
    };
    Role.prototype.getProxies = function (name) {
        if (this.client) {
            return this.client._getProxiesWithRole(name);
        }
        return [];
    };
    Role.prototype.sendTo = function (target, messageName, data) {
        if (!this.client) {
            throw new Error("Attempting to send from role that is not attached to client");
        }
        if (!this.client.isConnected) {
            throw new Error("Attempting to send from role with client that is not connected");
        }
        var e = glue_event_1.GlueEvent.createMessage(messageName, target.role, this.name, this.id, data);
        var envelope = glue_event_1.createEnvelope(e, [target.id]);
        this.client.sendData(envelope);
    };
    Role.prototype.sendToAll = function (roleName, messageName, data) {
        if (!this.client) {
            throw new Error("Attempting to send from role that is not attached to client");
        }
        if (!this.client.isConnected) {
            throw new Error("Attempting to send from role with client that is not connected");
        }
        var ids = this.client._getProxiesWithRole(roleName).map(function (p) { return p.id; });
        if (ids) {
            this.client.sendData(glue_event_1.createEnvelope(glue_event_1.GlueEvent.createMessage(messageName, roleName, this.name, this.id, data), ids));
        }
    };
    Role.prototype.requestFrom = function (target, msgName, data, handler) {
        if (!this.client) {
            throw new Error("Attempting to send from role that is not attached to client");
        }
        if (!this.client.isConnected) {
            throw new Error("Attempting to send from role with client that is not connected");
        }
        if (util_1.isUndefined(handler)) {
            if (util_1.isUndefined(data) || !util_1.isFunction(data)) {
                throw new Error("Requests need a response handler");
            }
            else {
                handler = data;
                data = null;
            }
        }
        var requestEvent;
        var requestId = utility_1.createId();
        requestEvent = glue_event_1.GlueEvent.createRequest(msgName, target.role, this.name, this.client.id, requestId, data);
        this.client._pendingRequests[requestId] = { responseHandler: handler, start: Date.now() };
        this.client.sendData(glue_event_1.createEnvelope(requestEvent, [target.id]));
    };
    Role.prototype.setMessageHandler = function (messageName, handler) {
        this._messageHandlers[messageName] = handler;
    };
    Role.prototype.setRequestHandler = function (requestName, handler) {
        this._requestHandlers[requestName] = handler;
    };
    Role.prototype.handleMessage = function (evt) {
        var handler = this._messageHandlers[evt.name];
        if (handler) {
            handler(evt.data, { id: evt.source, role: evt.sourceRole });
            return true;
        }
        else {
            return false;
        }
    };
    Role.prototype.handleRequest = function (evt) {
        var handler = this._requestHandlers[evt.name];
        var err = null;
        var data = undefined;
        if (handler) {
            try {
                data = handler(evt['data'], { id: evt.source, role: evt.sourceRole });
            }
            catch (ex) {
                err = request_error_1.RequestError.createRequestFailedError();
            }
        }
        else {
            err = request_error_1.RequestError.createUnhandledRequestError(evt);
        }
        return { data: data, err: err };
    };
    return Role;
}());
exports.Role = Role;
var Client = (function () {
    function Client(params, idGenerator, logger) {
        if (idGenerator === void 0) { idGenerator = function () { return utility_1.createId(); }; }
        if (logger === void 0) { logger = console; }
        var _this = this;
        this._estimatedLatency = -1;
        this._heartbeatTimeout = 10000;
        this._lastHeartbeatSent = Date.now();
        this._pendingHeartbeats = [];
        this._heartbeatInterval = 15000;
        this._maxMissedHeartbeats = 3;
        this._missedHeartbeats = 0;
        this._updateInterval = 100;
        this._roles = {};
        this._isConnected = false;
        this._requestTimeout = 1000;
        this._eventHandlers = {};
        this._onWelcome = noop;
        this._onClose = noop;
        this._onError = noop;
        this._onHeartbeatSent = noop;
        this._onHeartbeatReceived = noop;
        this._pendingRequests = {};
        this._roleSubscribers = {};
        this._proxies = [];
        this._isAutoReconnectEnabled = true;
        this._isReconnecting = false;
        this._reconnectDelay = 1000;
        this._maxReconnectAttempts = 10;
        this._reconnectAttempts = 0;
        this._reconnectAttemptHandler = noop;
        this._reconnectHandler = noop;
        this.logger = logger;
        if (params.url === undefined) {
            if (window !== undefined) {
                params.url = window.location.origin;
            }
            else {
                throw new Error('Non-browser clients must provide a url parameter');
            }
        }
        this._url = params.url;
        this._targetDomain = params.domain;
        this._roles = {};
        if (params.roles)
            params.roles.forEach(function (role) { _this.setRole(role); });
        this._id = idGenerator();
        this._context = params.context || null;
        this.initEventHandlers();
        try {
            this.connect(false);
        }
        catch (e) {
            this.logger.error("Client could not connect: " + e.message);
        }
    }
    Object.defineProperty(Client.prototype, "url", {
        get: function () { return this._url; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "id", {
        get: function () { return this._id; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "targetDomain", {
        get: function () { return this._targetDomain; },
        set: function (newName) { this._targetDomain = newName; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "context", {
        set: function (context) { this._context = context; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "estimatedLatency", {
        get: function () { return this._estimatedLatency; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "logger", {
        get: function () { return this._logger; },
        set: function (l) { this._logger = l; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "heartbeatTimeout", {
        get: function () { return this._heartbeatTimeout; },
        set: function (interval) { this._heartbeatTimeout = interval; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "heartbeatInterval", {
        get: function () { return this._heartbeatInterval; },
        set: function (newInterval) {
            this._heartbeatInterval = newInterval;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "maxMissedHeartbeats", {
        get: function () { return this._maxMissedHeartbeats; },
        set: function (newValue) { this._maxMissedHeartbeats = newValue; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "updateInterval", {
        get: function () { return this._updateInterval; },
        set: function (interval) { this._updateInterval = Math.max(interval, 0); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "roles", {
        get: function () { return this._roles; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "socket", {
        get: function () { return this._socket; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "isConnected", {
        get: function () { return this._isConnected; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "requestTimeout", {
        get: function () { return this._requestTimeout; },
        set: function (interval) { this._requestTimeout = interval; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "onWelcome", {
        set: function (action) { this._onWelcome = action; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "onClose", {
        set: function (handler) {
            this._onClose = handler;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "onError", {
        set: function (handler) {
            this._onError = handler;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "onHeartbeatSent", {
        set: function (handler) { this._onHeartbeatSent = handler; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "onHeartbeatReceived", {
        set: function (handler) { this._onHeartbeatReceived = handler; },
        enumerable: true,
        configurable: true
    });
    Client.prototype.connect = function (isReconnecting) {
        var _this = this;
        if (isReconnecting === void 0) { isReconnecting = false; }
        this._isReconnecting = isReconnecting;
        this._socket = new websocket_1.WS(this._url, 'glue');
        if (this._isReconnecting) {
            this._reconnectAttempts++;
            this._reconnectAttemptHandler();
        }
        this._socket.onopen = function () {
            _this._reconnectAttempts = 0;
            _this.socket.send(glue_event_1.createEnvelope(glue_event_1.GlueEvent.createConnect(_this.targetDomain, _this.id, Object.keys(_this.roles), _this._context)));
            _this._updateTimer = setInterval(function () { _this.update(); }, _this._updateInterval);
            if (_this._isReconnecting) {
                _this._reconnectHandler();
                _this._isReconnecting = false;
            }
        };
        this._socket.onmessage = function (m) {
            var e = JSON.parse(m.data);
            var handler = _this._eventHandlers[e.type];
            if (handler !== undefined) {
                handler(e);
            }
            else {
                _this.logger.warn('Unhandled message: ' + JSON.stringify(m));
            }
        };
        this._socket.onerror = function (err) {
            _this._onError(err);
        };
        this._socket.onclose = function (event) {
            _this._isConnected = false;
            var wasExpected = event.code === 1000;
            _this._onClose(wasExpected, event.code, event.reason);
            utility_1.forEachValue(_this.roles, function (role) {
                role.onClose(wasExpected, event.code, event.reason);
                _this._proxies.forEach(function (p) {
                    if (role.isSubscribed(p.role)) {
                        role.handleDisconnect(p);
                    }
                });
            });
            _this.cleanUp();
            if (_this._isAutoReconnectEnabled &&
                _this.reconnectAttempts < _this.maxReconnectAttempts &&
                reconnectionCloseCodes.indexOf(event.code) !== -1) {
                _this.scheduleReconnect();
            }
        };
    };
    Client.prototype.cleanUp = function () {
        clearInterval(this._updateTimer);
        this._pendingRequests = {};
        this._roleSubscribers = {};
        this._pendingHeartbeats = [];
        if (this._reconnectTimer)
            clearTimeout(this._reconnectTimer);
    };
    Client.prototype.initEventHandlers = function () {
        var _this = this;
        this._eventHandlers[glue_event_1.EventType.WELCOME] = function (evt) {
            _this._isConnected = true;
            _this._onWelcome(evt.domain);
            var roleNames = Object.keys(_this.roles);
            roleNames.forEach(function (name) {
                var role = _this.roles[name];
                role.onWelcome(evt.domain);
            });
        };
        this._eventHandlers[glue_event_1.EventType.MESSAGE] = function (evt) {
            var role = _this._roles[evt.targetRole];
            if (role) {
                if (!role.handleMessage(evt)) {
                    _this._logger.log(role.name + " cannot handle message: " + JSON.stringify(evt));
                }
            }
            else {
                _this._logger.log("Client needs role named " + evt.targetRole + " to handle message: " + JSON.stringify(evt));
            }
        };
        this._eventHandlers[glue_event_1.EventType.REQUEST] = function (evt) {
            _this.handleRequest(evt);
        };
        this._eventHandlers[glue_event_1.EventType.RESPONSE] = function (evt) {
            _this.handleResponse(evt);
        };
        this._eventHandlers[glue_event_1.EventType.HEARTBEAT] = function (evt) {
            _this._estimatedLatency = (Date.now() - evt.timestamp) / 2;
            var i = _this._pendingHeartbeats.indexOf(evt.timestamp);
            if (i !== -1) {
                utility_1.removeAtIndex(_this._pendingHeartbeats, i);
            }
            _this._missedHeartbeats = 0;
            _this._onHeartbeatReceived();
        };
        this._eventHandlers[glue_event_1.EventType.ROLE_CHANGED] = function (evt) {
            var subscribers = _this._roleSubscribers[evt.role];
            if (subscribers !== undefined) {
                if (evt.wasCreated) {
                    var proxy_1 = { id: evt.roleOwnerId, role: evt.role };
                    _this.cacheProxy(proxy_1);
                    subscribers.forEach(function (subscriber) { subscriber.handleConnect(proxy_1); });
                }
                else {
                    var proxy_2 = { id: evt.roleOwnerId, role: evt.role };
                    subscribers.forEach(function (subscriber) { subscriber.handleDisconnect(proxy_2); });
                    _this.clearProxy(evt.roleOwnerId);
                }
            }
            else {
                _this.logger.warn("Received ROLE_CHANGED event for role that has no subscriber: " + evt.role);
            }
        };
        this._eventHandlers[glue_event_1.EventType.ROLE_SUBSCRIPTION_RESPONSE] = function (evt) {
            var subscribers = _this._roleSubscribers[evt.role];
            if (subscribers) {
                var _loop_1 = function (i) {
                    var id = evt.roleOwners[i];
                    var proxy = { id: id, role: evt.role };
                    _this.cacheProxy(proxy);
                    subscribers.forEach(function (subscriber) { subscriber.handleConnect(proxy); });
                };
                for (var i = 0; i < evt.roleOwners.length; i++) {
                    _loop_1(i);
                }
            }
            else {
                _this.logger.warn("Received ROLE_SUBSCRIPITION_RESPONSE for role that client is not subcribed to: " + evt.role);
            }
        };
    };
    Client.prototype.closeConnection = function () {
        this._isReconnecting = false;
        if (this._reconnectTimer)
            clearTimeout(this._reconnectTimer);
        this._isConnected = false;
        var s = this.socket;
        if (s.readyState === ReadyState.OPEN || s.readyState === ReadyState.CONNECTING)
            this.socket.close(1000, 'Client closed');
    };
    Client.prototype.transferDomain = function (domainName) {
        if (!this._isConnected) {
            throw new Error("Cannot transfer domain if not connected");
        }
        this.cleanUp();
        this._targetDomain = domainName;
        var evt = glue_event_1.GlueEvent.createConnect(this.targetDomain, this.id, Object.keys(this.roles), this._context);
        this.sendData(glue_event_1.createEnvelope(evt));
        this._isConnected = false;
    };
    Client.prototype.update = function () {
        var now = Date.now();
        this.updateHearbeatTimers(now);
        this._updateRequests(now);
    };
    Client.prototype.updateHearbeatTimers = function (time) {
        while (this._pendingHeartbeats.length > 0) {
            if (time - this._pendingHeartbeats[0] > this.heartbeatTimeout) {
                this._missedHeartbeats++;
                this._pendingHeartbeats.splice(0, 1);
                if (this._missedHeartbeats >= this._maxMissedHeartbeats) {
                    this.socket.close(1006, 'Heartbeat missed');
                    return;
                }
            }
            else {
                break;
            }
        }
        if (time - this._lastHeartbeatSent >= this._heartbeatInterval && this.socket.readyState === ReadyState.OPEN) {
            this.sendData(glue_event_1.createEnvelope(glue_event_1.GlueEvent.createHeartbeat(time)));
            this._pendingHeartbeats.push(time);
            this._lastHeartbeatSent = time;
            this._onHeartbeatSent();
        }
    };
    Client.prototype.sendData = function (data) {
        switch (this.socket.readyState) {
            case ReadyState.OPEN:
                this.socket.send(data);
                break;
            case ReadyState.CONNECTING:
                this.logger.error('Attempting to send data through connecting socket: ' + data);
                break;
            case ReadyState.CLOSING:
                this.logger.error('Attempting to send data through closing socket: ' + data);
                break;
            case ReadyState.CLOSED:
                this.logger.error('Attempting to send data through closed socket: ' + data);
                break;
            default:
                break;
        }
    };
    Client.prototype.handleRequest = function (evt) {
        var role = this.roles[evt.targetRole];
        if (role) {
            var result = role.handleRequest(evt);
            var responseEvent = glue_event_1.GlueEvent.createResponse(evt.name, role.name, evt.targetRole, this.id, evt.requestId, result.data, result.err);
            this.sendData(glue_event_1.createEnvelope(responseEvent, [evt.source]));
        }
        else {
            this._logger.log("Client needs role named " + evt.targetRole + " to handle request: " + JSON.stringify(evt));
        }
    };
    Client.prototype.handleResponse = function (evt) {
        var request = this._pendingRequests[evt.requestId];
        if (request) {
            request.responseHandler(evt['data'], evt['error'], { id: evt.source, role: evt.sourceRole });
            delete this._pendingRequests[evt.requestId];
        }
        else {
            this.logger.error('No request for response: ' + JSON.stringify(evt));
        }
    };
    Client.prototype._updateRequests = function (time) {
        for (var pi in this._pendingRequests) {
            var pr = this._pendingRequests[pi];
            if (time - pr.start > this._requestTimeout) {
                pr.responseHandler(null, request_error_1.RequestError.createRequestTimeoutError(), null);
                delete this._pendingRequests[pi];
            }
        }
    };
    Client.prototype.cacheProxy = function (proxy) {
        if (!this.isCached(proxy)) {
            this._proxies.push(proxy);
        }
    };
    Client.prototype.clearProxy = function (id) {
        var i = -1;
        this._proxies.find(function (other, index) {
            if (id === other.id) {
                i = index;
                return true;
            }
            return false;
        });
        if (i >= 0) {
            this._proxies.splice(i, 1);
        }
    };
    Client.prototype.isCached = function (proxy) {
        var found = false;
        this._proxies.find(function (other) {
            if (proxy.id === other.id && proxy.role === other.role) {
                found = true;
                return true;
            }
            return false;
        });
        return found;
    };
    Client.prototype._getProxyWithRole = function (name) {
        var proxies = this._getProxiesWithRole(name);
        return proxies ? proxies[0] : null;
    };
    Client.prototype._getProxiesWithRole = function (name) {
        return this._proxies.filter(function (p) { return p.role === name; });
    };
    Client.prototype._subscribe = function (subscriber, subscription) {
        if (!this._roleSubscribers[subscription]) {
            this._roleSubscribers[subscription] = [];
            this.sendData(glue_event_1.createEnvelope(glue_event_1.GlueEvent.createRoleSubscribeEvent(subscription, this.id)));
        }
        this._roleSubscribers[subscription].push(subscriber);
    };
    Client.prototype._unsubscribe = function (subscriber, subscription) {
        var subscribers = this._roleSubscribers[subscription];
        if (subscribers) {
            var i = subscribers.indexOf(subscriber);
            if (i !== -1) {
                subscribers.splice(i, 1);
            }
            if (subscribers.length === 0) {
                this.sendData(glue_event_1.createEnvelope(glue_event_1.GlueEvent.createRoleUnsubscribeEvent(subscription, this.id)));
            }
        }
        else {
            this._logger.warn("Role " + subscriber.name + " is unsubscribing from " + subscription + ", but never subscribed in the first place");
        }
    };
    Client.prototype.setRole = function (role) {
        var oldRole = this._roles[role.name];
        if (!util_1.isUndefined(oldRole)) {
            oldRole.client = null;
            if (this.isConnected)
                this.sendData(glue_event_1.createEnvelope(glue_event_1.GlueEvent.createRoleChangedEvent(role.name, this._id, false)));
        }
        if (this.isConnected) {
            this.sendData(glue_event_1.createEnvelope(glue_event_1.GlueEvent.createRoleChangedEvent(role.name, this._id, true)));
        }
        this.roles[role.name] = role;
        role.client = this;
        return role;
    };
    Client.prototype.getRole = function (name) {
        return this.roles[name] || null;
    };
    Client.prototype.clearRole = function (roleName) {
        if (this.roles[roleName] !== undefined) {
            delete (this.roles[roleName]);
            if (this.isConnected) {
                this.sendData(glue_event_1.createEnvelope(glue_event_1.GlueEvent.createRoleChangedEvent(roleName, this._id, false)));
            }
        }
    };
    Object.defineProperty(Client.prototype, "isAutoReconnectEnabled", {
        get: function () { return this._isAutoReconnectEnabled; },
        set: function (val) { this._isAutoReconnectEnabled = val; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "reconnectDelay", {
        get: function () { return this._reconnectDelay; },
        set: function (delay) { this._reconnectDelay = delay; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "maxReconnectAttempts", {
        get: function () { return this._maxReconnectAttempts; },
        set: function (attempts) { this._maxReconnectAttempts = attempts; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "reconnectAttempts", {
        get: function () { return this._reconnectAttempts; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "onReconnectAttempt", {
        set: function (handler) { this._reconnectAttemptHandler = handler; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Client.prototype, "onReconnect", {
        set: function (handler) { this._reconnectHandler = handler; },
        enumerable: true,
        configurable: true
    });
    Client.prototype.scheduleReconnect = function () {
        var _this = this;
        this._reconnectTimer = setTimeout(function () { _this.connect(true); }, this.getBackoff());
    };
    Client.prototype.getBackoff = function () {
        return this._reconnectDelay * Math.pow(1.5, this._reconnectAttempts) + (this.reconnectDelay * Math.random());
    };
    Client.prototype.reconnect = function () {
        this.closeConnection();
        this.connect(true);
    };
    return Client;
}());
exports.Client = Client;
//# sourceMappingURL=client.js.map

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.WS = WebSocket;
//# sourceMappingURL=browser_websocket.js.map

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var EventType;
(function (EventType) {
    EventType[EventType["CONNECT"] = 0] = "CONNECT";
    EventType[EventType["WELCOME"] = 1] = "WELCOME";
    EventType[EventType["MESSAGE"] = 2] = "MESSAGE";
    EventType[EventType["REQUEST"] = 3] = "REQUEST";
    EventType[EventType["RESPONSE"] = 4] = "RESPONSE";
    EventType[EventType["HEARTBEAT"] = 5] = "HEARTBEAT";
    EventType[EventType["ROLE_CHANGED"] = 6] = "ROLE_CHANGED";
    EventType[EventType["ROLE_SUBSCRIBE"] = 7] = "ROLE_SUBSCRIBE";
    EventType[EventType["ROLE_UNSUBSCRIBE"] = 8] = "ROLE_UNSUBSCRIBE";
    EventType[EventType["ROLE_SUBSCRIPTION_RESPONSE"] = 9] = "ROLE_SUBSCRIPTION_RESPONSE";
})(EventType = exports.EventType || (exports.EventType = {}));
function createEnvelope(evt, targets) {
    if (targets === void 0) { targets = []; }
    var o = { event: evt, targets: targets };
    return JSON.stringify(o);
}
exports.createEnvelope = createEnvelope;
function openEnvelope(json) {
    var o = JSON.parse(json);
    return { event: o.event, targets: o.targets };
}
exports.openEnvelope = openEnvelope;
var GlueEvent = (function () {
    function GlueEvent() {
    }
    GlueEvent.createConnect = function (domain, id, roles, context) {
        return { type: EventType.CONNECT, domain: domain, id: id, roles: roles, context: context };
    };
    GlueEvent.createMessage = function (name, targetRole, sourceRole, source, data) {
        return { type: EventType.MESSAGE, targetRole: targetRole, source: source, sourceRole: sourceRole, name: name, data: data };
    };
    GlueEvent.createRequest = function (name, targetRole, sourceRole, source, requestId, data) {
        return { type: EventType.REQUEST, name: name, targetRole: targetRole, data: data, sourceRole: sourceRole, source: source, requestId: requestId };
    };
    GlueEvent.createResponse = function (name, targetRole, sourceRole, source, requestId, data, error) {
        return { type: EventType.RESPONSE, name: name, targetRole: targetRole, source: source, sourceRole: sourceRole, data: data, requestId: requestId, error: error };
    };
    GlueEvent.createWelcome = function (domain) {
        return { type: EventType.WELCOME, domain: domain };
    };
    GlueEvent.createHeartbeat = function (timestamp) {
        return { type: EventType.HEARTBEAT, timestamp: timestamp };
    };
    GlueEvent.createRoleChangedEvent = function (role, roleOwnerId, wasCreated) {
        return { type: EventType.ROLE_CHANGED, role: role, roleOwnerId: roleOwnerId, wasCreated: wasCreated };
    };
    GlueEvent.createRoleSubscribeEvent = function (role, clientId) {
        return { type: EventType.ROLE_SUBSCRIBE, role: role, subscriberId: clientId };
    };
    GlueEvent.createRoleUnsubscribeEvent = function (role, clientId) {
        return { type: EventType.ROLE_UNSUBSCRIBE, role: role, subscriberId: clientId };
    };
    GlueEvent.createRoleSubscriptionResponseEvent = function (role, roleOwners) {
        return { type: EventType.ROLE_SUBSCRIPTION_RESPONSE, role: role, roleOwners: roleOwners };
    };
    return GlueEvent;
}());
exports.GlueEvent = GlueEvent;
//# sourceMappingURL=glue_event.js.map

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var IdUpperBound = (Math.pow(2, 53) - 1);
function createId() {
    var id = 0;
    while (id === 0) {
        id = Math.floor(Math.random() * IdUpperBound);
    }
    return id;
}
exports.createId = createId;
function removeAtIndex(list, index) {
    list.splice(index, 1);
}
exports.removeAtIndex = removeAtIndex;
function removeElement(list, element) {
    if (list === undefined)
        return;
    var i = list.indexOf(element);
    if (i !== -1) {
        list.splice(i, 1);
    }
}
exports.removeElement = removeElement;
function removeFirstMatchingElement(list, pred) {
    if (list === undefined)
        return undefined;
    for (var i = 0; i < list.length; i++) {
        var el = list[i];
        if (pred(el)) {
            list.splice(i, 1);
            return el;
        }
    }
    return undefined;
}
exports.removeFirstMatchingElement = removeFirstMatchingElement;
function forEachValue(obj, fn) {
    var keys = Object.keys(obj);
    var l = keys.length;
    for (var i = 0; i < l; i++) {
        fn(obj[keys[i]]);
    }
}
exports.forEachValue = forEachValue;
//# sourceMappingURL=utility.js.map

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var ErrorType;
(function (ErrorType) {
    ErrorType[ErrorType["REQUEST_TIMEOUT"] = 0] = "REQUEST_TIMEOUT";
    ErrorType[ErrorType["UNHANDLED_REQUEST"] = 1] = "UNHANDLED_REQUEST";
    ErrorType[ErrorType["REQUEST_FAILED"] = 2] = "REQUEST_FAILED";
})(ErrorType = exports.ErrorType || (exports.ErrorType = {}));
var RequestError = (function () {
    function RequestError(type, reason) {
        this.type = type;
        this.reason = reason;
    }
    RequestError.createRequestTimeoutError = function () {
        return new RequestError(ErrorType.REQUEST_TIMEOUT, "Request timed out");
    };
    ;
    RequestError.createRequestFailedError = function () {
        return new RequestError(ErrorType.REQUEST_FAILED, "Request could not be completed due to error at callee");
    };
    ;
    RequestError.createUnhandledRequestError = function (unhandledRequest) {
        return new RequestError(ErrorType.UNHANDLED_REQUEST, "Client has no handler for request: " + unhandledRequest.name);
    };
    ;
    return RequestError;
}());
exports.RequestError = RequestError;
//# sourceMappingURL=request_error.js.map

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global, process) {// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

var formatRegExp = /%[sdj%]/g;
exports.format = function(f) {
  if (!isString(f)) {
    var objects = [];
    for (var i = 0; i < arguments.length; i++) {
      objects.push(inspect(arguments[i]));
    }
    return objects.join(' ');
  }

  var i = 1;
  var args = arguments;
  var len = args.length;
  var str = String(f).replace(formatRegExp, function(x) {
    if (x === '%%') return '%';
    if (i >= len) return x;
    switch (x) {
      case '%s': return String(args[i++]);
      case '%d': return Number(args[i++]);
      case '%j':
        try {
          return JSON.stringify(args[i++]);
        } catch (_) {
          return '[Circular]';
        }
      default:
        return x;
    }
  });
  for (var x = args[i]; i < len; x = args[++i]) {
    if (isNull(x) || !isObject(x)) {
      str += ' ' + x;
    } else {
      str += ' ' + inspect(x);
    }
  }
  return str;
};


// Mark that a method should not be used.
// Returns a modified function which warns once by default.
// If --no-deprecation is set, then it is a no-op.
exports.deprecate = function(fn, msg) {
  // Allow for deprecating things in the process of starting up.
  if (isUndefined(global.process)) {
    return function() {
      return exports.deprecate(fn, msg).apply(this, arguments);
    };
  }

  if (process.noDeprecation === true) {
    return fn;
  }

  var warned = false;
  function deprecated() {
    if (!warned) {
      if (process.throwDeprecation) {
        throw new Error(msg);
      } else if (process.traceDeprecation) {
        console.trace(msg);
      } else {
        console.error(msg);
      }
      warned = true;
    }
    return fn.apply(this, arguments);
  }

  return deprecated;
};


var debugs = {};
var debugEnviron;
exports.debuglog = function(set) {
  if (isUndefined(debugEnviron))
    debugEnviron = process.env.NODE_DEBUG || '';
  set = set.toUpperCase();
  if (!debugs[set]) {
    if (new RegExp('\\b' + set + '\\b', 'i').test(debugEnviron)) {
      var pid = process.pid;
      debugs[set] = function() {
        var msg = exports.format.apply(exports, arguments);
        console.error('%s %d: %s', set, pid, msg);
      };
    } else {
      debugs[set] = function() {};
    }
  }
  return debugs[set];
};


/**
 * Echos the value of a value. Trys to print the value out
 * in the best way possible given the different types.
 *
 * @param {Object} obj The object to print out.
 * @param {Object} opts Optional options object that alters the output.
 */
/* legacy: obj, showHidden, depth, colors*/
function inspect(obj, opts) {
  // default options
  var ctx = {
    seen: [],
    stylize: stylizeNoColor
  };
  // legacy...
  if (arguments.length >= 3) ctx.depth = arguments[2];
  if (arguments.length >= 4) ctx.colors = arguments[3];
  if (isBoolean(opts)) {
    // legacy...
    ctx.showHidden = opts;
  } else if (opts) {
    // got an "options" object
    exports._extend(ctx, opts);
  }
  // set default options
  if (isUndefined(ctx.showHidden)) ctx.showHidden = false;
  if (isUndefined(ctx.depth)) ctx.depth = 2;
  if (isUndefined(ctx.colors)) ctx.colors = false;
  if (isUndefined(ctx.customInspect)) ctx.customInspect = true;
  if (ctx.colors) ctx.stylize = stylizeWithColor;
  return formatValue(ctx, obj, ctx.depth);
}
exports.inspect = inspect;


// http://en.wikipedia.org/wiki/ANSI_escape_code#graphics
inspect.colors = {
  'bold' : [1, 22],
  'italic' : [3, 23],
  'underline' : [4, 24],
  'inverse' : [7, 27],
  'white' : [37, 39],
  'grey' : [90, 39],
  'black' : [30, 39],
  'blue' : [34, 39],
  'cyan' : [36, 39],
  'green' : [32, 39],
  'magenta' : [35, 39],
  'red' : [31, 39],
  'yellow' : [33, 39]
};

// Don't use 'blue' not visible on cmd.exe
inspect.styles = {
  'special': 'cyan',
  'number': 'yellow',
  'boolean': 'yellow',
  'undefined': 'grey',
  'null': 'bold',
  'string': 'green',
  'date': 'magenta',
  // "name": intentionally not styling
  'regexp': 'red'
};


function stylizeWithColor(str, styleType) {
  var style = inspect.styles[styleType];

  if (style) {
    return '\u001b[' + inspect.colors[style][0] + 'm' + str +
           '\u001b[' + inspect.colors[style][1] + 'm';
  } else {
    return str;
  }
}


function stylizeNoColor(str, styleType) {
  return str;
}


function arrayToHash(array) {
  var hash = {};

  array.forEach(function(val, idx) {
    hash[val] = true;
  });

  return hash;
}


function formatValue(ctx, value, recurseTimes) {
  // Provide a hook for user-specified inspect functions.
  // Check that value is an object with an inspect function on it
  if (ctx.customInspect &&
      value &&
      isFunction(value.inspect) &&
      // Filter out the util module, it's inspect function is special
      value.inspect !== exports.inspect &&
      // Also filter out any prototype objects using the circular check.
      !(value.constructor && value.constructor.prototype === value)) {
    var ret = value.inspect(recurseTimes, ctx);
    if (!isString(ret)) {
      ret = formatValue(ctx, ret, recurseTimes);
    }
    return ret;
  }

  // Primitive types cannot have properties
  var primitive = formatPrimitive(ctx, value);
  if (primitive) {
    return primitive;
  }

  // Look up the keys of the object.
  var keys = Object.keys(value);
  var visibleKeys = arrayToHash(keys);

  if (ctx.showHidden) {
    keys = Object.getOwnPropertyNames(value);
  }

  // IE doesn't make error fields non-enumerable
  // http://msdn.microsoft.com/en-us/library/ie/dww52sbt(v=vs.94).aspx
  if (isError(value)
      && (keys.indexOf('message') >= 0 || keys.indexOf('description') >= 0)) {
    return formatError(value);
  }

  // Some type of object without properties can be shortcutted.
  if (keys.length === 0) {
    if (isFunction(value)) {
      var name = value.name ? ': ' + value.name : '';
      return ctx.stylize('[Function' + name + ']', 'special');
    }
    if (isRegExp(value)) {
      return ctx.stylize(RegExp.prototype.toString.call(value), 'regexp');
    }
    if (isDate(value)) {
      return ctx.stylize(Date.prototype.toString.call(value), 'date');
    }
    if (isError(value)) {
      return formatError(value);
    }
  }

  var base = '', array = false, braces = ['{', '}'];

  // Make Array say that they are Array
  if (isArray(value)) {
    array = true;
    braces = ['[', ']'];
  }

  // Make functions say that they are functions
  if (isFunction(value)) {
    var n = value.name ? ': ' + value.name : '';
    base = ' [Function' + n + ']';
  }

  // Make RegExps say that they are RegExps
  if (isRegExp(value)) {
    base = ' ' + RegExp.prototype.toString.call(value);
  }

  // Make dates with properties first say the date
  if (isDate(value)) {
    base = ' ' + Date.prototype.toUTCString.call(value);
  }

  // Make error with message first say the error
  if (isError(value)) {
    base = ' ' + formatError(value);
  }

  if (keys.length === 0 && (!array || value.length == 0)) {
    return braces[0] + base + braces[1];
  }

  if (recurseTimes < 0) {
    if (isRegExp(value)) {
      return ctx.stylize(RegExp.prototype.toString.call(value), 'regexp');
    } else {
      return ctx.stylize('[Object]', 'special');
    }
  }

  ctx.seen.push(value);

  var output;
  if (array) {
    output = formatArray(ctx, value, recurseTimes, visibleKeys, keys);
  } else {
    output = keys.map(function(key) {
      return formatProperty(ctx, value, recurseTimes, visibleKeys, key, array);
    });
  }

  ctx.seen.pop();

  return reduceToSingleString(output, base, braces);
}


function formatPrimitive(ctx, value) {
  if (isUndefined(value))
    return ctx.stylize('undefined', 'undefined');
  if (isString(value)) {
    var simple = '\'' + JSON.stringify(value).replace(/^"|"$/g, '')
                                             .replace(/'/g, "\\'")
                                             .replace(/\\"/g, '"') + '\'';
    return ctx.stylize(simple, 'string');
  }
  if (isNumber(value))
    return ctx.stylize('' + value, 'number');
  if (isBoolean(value))
    return ctx.stylize('' + value, 'boolean');
  // For some reason typeof null is "object", so special case here.
  if (isNull(value))
    return ctx.stylize('null', 'null');
}


function formatError(value) {
  return '[' + Error.prototype.toString.call(value) + ']';
}


function formatArray(ctx, value, recurseTimes, visibleKeys, keys) {
  var output = [];
  for (var i = 0, l = value.length; i < l; ++i) {
    if (hasOwnProperty(value, String(i))) {
      output.push(formatProperty(ctx, value, recurseTimes, visibleKeys,
          String(i), true));
    } else {
      output.push('');
    }
  }
  keys.forEach(function(key) {
    if (!key.match(/^\d+$/)) {
      output.push(formatProperty(ctx, value, recurseTimes, visibleKeys,
          key, true));
    }
  });
  return output;
}


function formatProperty(ctx, value, recurseTimes, visibleKeys, key, array) {
  var name, str, desc;
  desc = Object.getOwnPropertyDescriptor(value, key) || { value: value[key] };
  if (desc.get) {
    if (desc.set) {
      str = ctx.stylize('[Getter/Setter]', 'special');
    } else {
      str = ctx.stylize('[Getter]', 'special');
    }
  } else {
    if (desc.set) {
      str = ctx.stylize('[Setter]', 'special');
    }
  }
  if (!hasOwnProperty(visibleKeys, key)) {
    name = '[' + key + ']';
  }
  if (!str) {
    if (ctx.seen.indexOf(desc.value) < 0) {
      if (isNull(recurseTimes)) {
        str = formatValue(ctx, desc.value, null);
      } else {
        str = formatValue(ctx, desc.value, recurseTimes - 1);
      }
      if (str.indexOf('\n') > -1) {
        if (array) {
          str = str.split('\n').map(function(line) {
            return '  ' + line;
          }).join('\n').substr(2);
        } else {
          str = '\n' + str.split('\n').map(function(line) {
            return '   ' + line;
          }).join('\n');
        }
      }
    } else {
      str = ctx.stylize('[Circular]', 'special');
    }
  }
  if (isUndefined(name)) {
    if (array && key.match(/^\d+$/)) {
      return str;
    }
    name = JSON.stringify('' + key);
    if (name.match(/^"([a-zA-Z_][a-zA-Z_0-9]*)"$/)) {
      name = name.substr(1, name.length - 2);
      name = ctx.stylize(name, 'name');
    } else {
      name = name.replace(/'/g, "\\'")
                 .replace(/\\"/g, '"')
                 .replace(/(^"|"$)/g, "'");
      name = ctx.stylize(name, 'string');
    }
  }

  return name + ': ' + str;
}


function reduceToSingleString(output, base, braces) {
  var numLinesEst = 0;
  var length = output.reduce(function(prev, cur) {
    numLinesEst++;
    if (cur.indexOf('\n') >= 0) numLinesEst++;
    return prev + cur.replace(/\u001b\[\d\d?m/g, '').length + 1;
  }, 0);

  if (length > 60) {
    return braces[0] +
           (base === '' ? '' : base + '\n ') +
           ' ' +
           output.join(',\n  ') +
           ' ' +
           braces[1];
  }

  return braces[0] + base + ' ' + output.join(', ') + ' ' + braces[1];
}


// NOTE: These type checking functions intentionally don't use `instanceof`
// because it is fragile and can be easily faked with `Object.create()`.
function isArray(ar) {
  return Array.isArray(ar);
}
exports.isArray = isArray;

function isBoolean(arg) {
  return typeof arg === 'boolean';
}
exports.isBoolean = isBoolean;

function isNull(arg) {
  return arg === null;
}
exports.isNull = isNull;

function isNullOrUndefined(arg) {
  return arg == null;
}
exports.isNullOrUndefined = isNullOrUndefined;

function isNumber(arg) {
  return typeof arg === 'number';
}
exports.isNumber = isNumber;

function isString(arg) {
  return typeof arg === 'string';
}
exports.isString = isString;

function isSymbol(arg) {
  return typeof arg === 'symbol';
}
exports.isSymbol = isSymbol;

function isUndefined(arg) {
  return arg === void 0;
}
exports.isUndefined = isUndefined;

function isRegExp(re) {
  return isObject(re) && objectToString(re) === '[object RegExp]';
}
exports.isRegExp = isRegExp;

function isObject(arg) {
  return typeof arg === 'object' && arg !== null;
}
exports.isObject = isObject;

function isDate(d) {
  return isObject(d) && objectToString(d) === '[object Date]';
}
exports.isDate = isDate;

function isError(e) {
  return isObject(e) &&
      (objectToString(e) === '[object Error]' || e instanceof Error);
}
exports.isError = isError;

function isFunction(arg) {
  return typeof arg === 'function';
}
exports.isFunction = isFunction;

function isPrimitive(arg) {
  return arg === null ||
         typeof arg === 'boolean' ||
         typeof arg === 'number' ||
         typeof arg === 'string' ||
         typeof arg === 'symbol' ||  // ES6 symbol
         typeof arg === 'undefined';
}
exports.isPrimitive = isPrimitive;

exports.isBuffer = __webpack_require__(8);

function objectToString(o) {
  return Object.prototype.toString.call(o);
}


function pad(n) {
  return n < 10 ? '0' + n.toString(10) : n.toString(10);
}


var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep',
              'Oct', 'Nov', 'Dec'];

// 26 Feb 16:19:34
function timestamp() {
  var d = new Date();
  var time = [pad(d.getHours()),
              pad(d.getMinutes()),
              pad(d.getSeconds())].join(':');
  return [d.getDate(), months[d.getMonth()], time].join(' ');
}


// log is just a thin wrapper to console.log that prepends a timestamp
exports.log = function() {
  console.log('%s - %s', timestamp(), exports.format.apply(exports, arguments));
};


/**
 * Inherit the prototype methods from one constructor into another.
 *
 * The Function.prototype.inherits from lang.js rewritten as a standalone
 * function (not on Function.prototype). NOTE: If this file is to be loaded
 * during bootstrapping this function needs to be rewritten using some native
 * functions as prototype setup using normal JavaScript does not work as
 * expected during bootstrapping (see mirror.js in r114903).
 *
 * @param {function} ctor Constructor function which needs to inherit the
 *     prototype.
 * @param {function} superCtor Constructor function to inherit prototype from.
 */
exports.inherits = __webpack_require__(9);

exports._extend = function(origin, add) {
  // Don't do anything if add isn't an object
  if (!add || !isObject(add)) return origin;

  var keys = Object.keys(add);
  var i = keys.length;
  while (i--) {
    origin[keys[i]] = add[keys[i]];
  }
  return origin;
};

function hasOwnProperty(obj, prop) {
  return Object.prototype.hasOwnProperty.call(obj, prop);
}

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(6), __webpack_require__(7)))

/***/ }),
/* 6 */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
	// This works if the window reference is available
	if(typeof window === "object")
		g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),
/* 7 */
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };


/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = function isBuffer(arg) {
  return arg && typeof arg === 'object'
    && typeof arg.copy === 'function'
    && typeof arg.fill === 'function'
    && typeof arg.readUInt8 === 'function';
}

/***/ }),
/* 9 */
/***/ (function(module, exports) {

if (typeof Object.create === 'function') {
  // implementation from standard node.js 'util' module
  module.exports = function inherits(ctor, superCtor) {
    ctor.super_ = superCtor
    ctor.prototype = Object.create(superCtor.prototype, {
      constructor: {
        value: ctor,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
  };
} else {
  // old school shim for old browsers
  module.exports = function inherits(ctor, superCtor) {
    ctor.super_ = superCtor
    var TempCtor = function () {}
    TempCtor.prototype = superCtor.prototype
    ctor.prototype = new TempCtor()
    ctor.prototype.constructor = ctor
  }
}


/***/ })
/******/ ]);
});