import { RequestError } from "./request_error";
export declare enum EventType {
    CONNECT = 0,
    WELCOME = 1,
    MESSAGE = 2,
    REQUEST = 3,
    RESPONSE = 4,
    HEARTBEAT = 5,
    ROLE_CHANGED = 6,
    ROLE_SUBSCRIBE = 7,
    ROLE_UNSUBSCRIBE = 8,
    ROLE_SUBSCRIPTION_RESPONSE = 9,
}
export declare function createEnvelope(evt: GlueEvent, targets?: Array<number>): string;
export declare function openEnvelope(json: string): {
    event: GlueEvent;
    targets: Array<number>;
};
export interface GlueEvent {
    type: EventType;
}
export interface MessageEvent extends GlueEvent {
    name: string;
    targetRole: string;
    sourceRole: string;
    source: number;
    data: any | null;
}
export interface RequestEvent extends GlueEvent {
    name: string;
    targetRole: string;
    sourceRole: string;
    source: number;
    requestId: number;
    data: any | null;
}
export interface ResponseEvent extends GlueEvent {
    name: string;
    targetRole: string;
    sourceRole: string;
    source: number;
    requestId: number;
    data: any | null;
    error: RequestError | null;
}
export interface ConnectEvent extends GlueEvent {
    domain: string;
    id: number;
    roles: Array<string>;
    context: any;
}
export interface WelcomeEvent extends GlueEvent {
    domain: string;
}
export interface HeartbeatEvent extends GlueEvent {
    timestamp: number;
}
export interface RoleChangedEvent extends GlueEvent {
    role: string;
    roleOwnerId: number;
    wasCreated: boolean;
}
export interface RoleSubscribeEvent extends GlueEvent {
    role: string;
    subscriberId: number;
}
export interface RoleSubscriptionResponseEvent extends GlueEvent {
    role: string;
    roleOwners: Array<number>;
}
export interface RoleUnsubscribeEvent extends GlueEvent {
    role: string;
    subscriberId: number;
}
export declare class GlueEvent implements GlueEvent {
    static createConnect(domain: string, id: number, roles: Array<string>, context?: any): ConnectEvent;
    static createMessage(name: string, targetRole: string, sourceRole: string, source: number, data: any): MessageEvent;
    static createRequest(name: string, targetRole: string, sourceRole: string, source: number, requestId: number, data: any): RequestEvent;
    static createResponse(name: string, targetRole: string, sourceRole: string, source: number, requestId: number, data: any, error: RequestError | null): ResponseEvent;
    static createWelcome(domain: string): WelcomeEvent;
    static createHeartbeat(timestamp: number): HeartbeatEvent;
    static createRoleChangedEvent(role: string, roleOwnerId: number, wasCreated: boolean): RoleChangedEvent;
    static createRoleSubscribeEvent(role: string, clientId: number): RoleSubscribeEvent;
    static createRoleUnsubscribeEvent(role: string, clientId: number): RoleUnsubscribeEvent;
    static createRoleSubscriptionResponseEvent(role: string, roleOwners: Array<number>): RoleSubscriptionResponseEvent;
}
