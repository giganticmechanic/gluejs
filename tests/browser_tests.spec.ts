import {expect} from 'chai';
import {Client, Role} from "../src/client";
import {ErrorType, RequestError} from "../src/request_error";
import {RoleProxy} from "../src";

const nullLogger = {log:()=>{}, warn:()=>{}, error:()=>{}};

const TIMEOUT = 500;
const POLL_INTERVAL = 0;

function eventually(condition:()=>boolean, timeout:number=TIMEOUT, pollInterval:number=POLL_INTERVAL):Promise<void> {
    return new Promise((resolve:()=>void, reject:(err:Error)=>void)=>{
        const start = Date.now();
        const check = ()=>{
            const elapsed = Date.now() - start;
            if (elapsed < timeout) {
                if (condition()) {
                    resolve();
                } else {
                    setTimeout(check, pollInterval);
                }
            } else {
                reject(new Error('Eventually Timed out'));
            }
        };
        check();
    });
}

function never(condition:()=>boolean, timeout:number=TIMEOUT, pollInterval:number=POLL_INTERVAL):Promise<void> {
    return new Promise((resolve:()=>void, reject:(err:Error)=>void)=>{
        const start = Date.now();
        const check = ()=>{
            const elapsed = Date.now() - start;
            if (elapsed < timeout) {
                if (condition()) {
                    reject(new Error('Never predicate was true'));
                } else {
                    setTimeout(check, pollInterval);
                }
            } else {
                resolve();
            }
        };
        check();
    });
}

function wait(timeout:number):Promise<void> {
    return new Promise((resolve)=>{
        setTimeout(resolve, timeout);
    });
}

function simulateError(c:Client) {
    const s:any = c.socket;
    s.onerror(new Error('Simulated error'));
    s.close(4999, 'Simulated error');
}

const url = "ws://127.0.0.1:5000";

describe('Client', function(){

    class Greeter extends Role {

        sentMessageData: any;
        sentRequestData: any;
        receivedResponseWithoutData = false;
        receivedResponseWithData = false;
        response: any;

        constructor() {
            super('Greeter');
        }

        onWelcome = (domain: string) => {
            this.subscribe('Listener', listener=>{
                this.sendTo(listener, 'hello');
                this.sendTo(listener,'hello with data', this.sentMessageData = 'HELLO');
                this.requestFrom(listener, 'request', (response:any, err:RequestError)=>{
                    this.receivedResponseWithoutData = true;
                });
                this.requestFrom(listener,'request with data', this.sentRequestData = 'data', (response:any, err:RequestError)=>{
                    this.receivedResponseWithData = true;
                    this.response = response;
                });
            });
        }
    }

    class Listener extends Role {
        heardHello = false;
        heardData: any;
        requestData:any;
        constructor() {
            super('Listener');
            this.setMessageHandler('hello', ()=>{this.heardHello = true;});
            this.setMessageHandler('hello with data', (data)=>{this.heardData = data;});
            this.setRequestHandler('request', data => { return {success:true} });
            this.setRequestHandler('request with data', data => { this.requestData = data; return {success:true} });
        }
    }

    describe('Domains', function() {

        it ('allows clients in the same domain to communicate', function() {
            const domain = this.test.title;

            const c1 = new Client({url: url, domain: domain});
            const c2 = new Client({url: url, domain: domain});

            const a = new Role('A');
            a.onWelcome = ()=> {
                a.subscribe('B', r=>a.sendTo(r, 'hello'));
            };
            c1.setRole(a);

            let heardHello = false;
            const b = new Role('B');
            b.setMessageHandler('hello', ()=>heardHello = true);
            c2.setRole(b);

            return eventually(() => heardHello);
        });

        it ('prevents clients in different domains from communicating', function() {
            const domain1 = this.test.title + "1";
            const domain2 = this.test.title + "2";

            const c1 = new Client({url: url, domain: domain1, roles:[new Listener()]});
            const c2 = new Client({url: url, domain: domain2, roles:[new Greeter()]});

            return never(()=>{
                const l = c1.getRole('Listener') as Listener;
                return l.heardHello;
            });
        });

        it ('can transfer domains', function() {
            const domain1 = this.test.title + '1';
            const domain2 = this.test.title + '2';
            const c = new Client({url:url, domain:domain1});

            let transfered = false;
            c.onWelcome = ()=>{
                c.transferDomain(domain2);
                transfered = c.targetDomain === domain2;
            };
            return eventually(()=>transfered);
        });

        it ('disconnects when transfering domain', function(done) {
            const domain1 = this.test.title + '1';
            const domain2 = this.test.title + '2';
            const c = new Client({url:url, domain:domain1});

            c.onWelcome = ()=>{
                if (c.targetDomain === domain1) {
                    c.transferDomain(domain2);
                    expect(c.isConnected).to.be.false;
                    done();
                }
            };
        });

        it ('calls onWelcome when transfering domains', function() {
            const domain1 = this.test.title + '1';
            const domain2 = this.test.title + '2';
            const c = new Client({url:url, domain:domain1});

            let timesCalled = 0;

            c.onWelcome = ()=> {
                timesCalled++;
                c.transferDomain(domain2);
            };

            c.onClose = (expected, code, reason)=>{
                console.log(`expected: ${expected}, code: ${code}, reason: ${reason}`)
            };
            return eventually(()=>timesCalled === 2);
        });

        it ('throws an exception if transferDomain called when not connected', function() {
            const domain1 = this.test.title + '1';
            const domain2 = this.test.title + '2';
            const c = new Client({url:url, domain:domain1});
            expect(()=>c.transferDomain(domain2)).to.throw();
        });
    });

    describe('Connecting', function() {

        it ('connects on creation', function() {
            const client = new Client({url:url, domain:this.test.title});
            return eventually(()=>client.isConnected);
        });

        it ('calls onError if it cannot connect', function() {
            const c = new Client({url:'ws://not.a.valid.url', domain:this.test.title});
            let didError = false;
            c.onError = ()=>{ didError = true };
            return eventually(()=>didError);
        });

        it ('calls onClose if it cannot connect', function() {
            const c = new Client({url:'ws://not.a.valid.url', domain:this.test.title});
            let didClose = false;
            c.onClose = ()=>{ didClose = true; };
            return eventually(()=>didClose);
        });

        it ('has id on creation', function() {
            const c = new Client({url:url, domain:this.test.title});
            return eventually(()=>c.id > 0);
        });

        it ('correctly reports disconnected status', function() {
            const c = new Client({url:url, domain:this.test.title});

            return eventually(()=>c.isConnected)
                .then(()=>{
                    c.closeConnection();
                    expect(c.isConnected).to.be.false;
                });
        });

        it ('is connected after onWelcome', function() {
            const c = new Client({url:url, domain:this.test.title});
            let success = false;
            c.onWelcome = ()=>{
                success = c.isConnected;
            };
            return eventually(()=>success);
        });

        it ('receives domain name onWelcome', function() {
            const c = new Client({url:url, domain:this.test.title});
            let success = false;
            c.onWelcome = (domainName)=>{
                success = domainName === this.test.title;
            };
            return eventually(()=>success);
        });

        it ('can send context for router', function() {
            const c = new Client({url:url, domain:this.test.title, context:{shouldFail:false}});
            let success = false;
            c.onWelcome = ()=>{ success = true };
            return eventually(()=>success);
        });

        it ('can fail to connect', function() {
            const c = new Client({url:url, domain:this.test.title, context:{shouldFail:true}});
            let failed = false;
            c.onClose = () => {
                failed = true
            };
            return eventually(()=>failed);
        });

    });

    describe('Closing', function() {

        it ('closes when closeConnection is called', function(){
            const c = new Client({url:url, domain:this.test.title});

            return eventually(()=>c.isConnected)
                .then(()=>{
                    c.closeConnection();
                    return eventually(()=>!c.isConnected);
                });
        });

        it ('onClose called when error occurs', function() {
            const c = new Client({url:'ws://not.a.valid.url', domain:this.test.title});
            c.isAutoReconnectEnabled = false;

            let closed = false;
            let error = false;

            c.onError = ()=>{ error = true };
            c.onClose = (e, c, r)=>{ closed = true };

            return eventually(()=> closed && error);
        });

        it('onClose is called after closeConnection is called', function() {
            const c = new Client({url:url, domain:this.test.title});
            let closed = false;
            c.onClose = ()=>{closed = true;};
            return eventually(()=>c.isConnected)
                .then(()=>{
                    c.closeConnection();
                    return eventually(()=>closed);
                });
        });

        it('closes from closeConnection are expected', function() {
            const c = new Client({url:url, domain:this.test.title});
            let expected = false;
            c.onClose = (e)=>{expected = e};
            c.onWelcome = ()=>{c.closeConnection()};
            return eventually(()=>expected);
        });

        it('closes due to errors are unexpected', function(done) {
            const c = new Client({url:'ws://not.a.valid.url', domain:this.test.title});
            c.onClose = (expected)=>{
                expect(expected).to.be.false;
                done();
            };
        });

    });

    describe('Messages', function() {

        it('can send a message', function() {
            const listener = new Client({url:url, domain:this.test.title, roles:[new Listener()]});
            const greeter = new Client({url:url, domain:this.test.title, roles:[new Greeter()]});

            return eventually(() => {
                const l = listener.getRole('Listener') as Listener;
                return l.heardHello;
            });
        });

        it ('does not receive messages it targets to itself', function() {
            const c = new Client({ url:url, domain:this.test.title });
            const listener = c.setRole(new Listener()) as Listener;
            // TODO: Move send all into the roles and then use sendAll to implement this
            listener.onWelcome = ()=>{
                listener.subscribe('Listener', l=>{this.sendTo(l,'hello')});
            };

            return never(() => listener.heardHello);
        });

        it('can send data with message', function() {
            const greeter = new Client({ url:url, domain:this.test.title, roles:[new Greeter()] });
            const listener = new Client({ url:url, domain:this.test.title,  roles:[new Listener()] });

            const g = greeter.getRole('Greeter') as Greeter;
            const l = listener.getRole('Listener') as Listener;

            return eventually(()=>l.heardData === g.sentMessageData);
        });

        it('receives the sender as a role proxy', function() {
            const c1 = new Client({ url: url, domain:this.test.title });
            const c2 = new Client({ url: url, domain:this.test.title });

            const a = new Role('A');
            const b = new Role('B');

            let success = false;

            b.setMessageHandler('hello', (data:undefined, source:RoleProxy)=>{
                success = (source.id === a.id && source.role === 'A');
            });

            a.onWelcome = () => {
                a.subscribe(b.name, (bProxy:RoleProxy)=>{
                    a.sendTo(bProxy, 'hello');
                });
            };

            c1.setRole(a);
            c2.setRole(b);

            return eventually(()=>success);
        });

    });

    describe('Requests', function(){

        it ('can set request timeout', function(){
            const c = new Client({url:url, domain:this.test.title});
            c.requestTimeout = 10;
            expect(c.requestTimeout).to.equal(10);
        });

        it ('receives unhandled error if target cannot handle request', function() {
            let errorType:ErrorType;

            const a = new Role('A');
            const b = new Role('B');

            a.onWelcome = ()=> {
                a.subscribe(b.name, r => a.requestFrom(r, 'Test', (data:any, err:RequestError)=> {
                    if (err) errorType = err.type;
                }));
            };

            const c1 = new Client({ url:url, domain:this.test.title, roles:[a] });
            const c2 = new Client({ url:url, domain:this.test.title, roles:[b] });

            return eventually(()=>errorType===ErrorType.UNHANDLED_REQUEST);
        });

        it('receives failed error if target has exception', function(){
            const aRole = new Role('A');
            aRole.onWelcome = () => {
                aRole.subscribe('B', b => {
                    aRole.requestFrom(b, 'Test', (data:any, err:RequestError)=>{
                        errType = err.type;
                    });
                });
            };

            const bRole = new Role('B');
            bRole.setRequestHandler('Test', ()=>{ throw new Error("BLARRGHH!!");});

            const c1 = new Client({url:url, domain:this.test.title, roles:[aRole]});
            const c2 = new Client({url:url, domain:this.test.title, roles:[bRole]});

            c2.logger = nullLogger;

            let errType:ErrorType;

            return eventually(()=>{return errType === ErrorType.REQUEST_FAILED;});
        });

        it ('can send data with request', function() {
            const c1 = new Client({url: url, domain: this.test.title});
            const greeter = c1.setRole(new Greeter()) as Greeter;

            const c2 = new Client({url: url, domain: this.test.title, roles:[new Listener()]});
            const listener = c2.setRole(new Listener());

            return eventually(()=>greeter.receivedResponseWithData);
        });

        it ("receives sender as role proxy", function() {
            const c1 = new Client({url:url, domain:this.test.title});
            const c2 = new Client({url:url, domain:this.test.title});

            const a = new Role('A');
            const b = new Role('B');

            let success = false;
            a.onWelcome = ()=>{
                a.subscribe('B', (bProxy)=>{
                    a.requestFrom(bProxy, 'Name', (data:undefined, err:RequestError, source:RoleProxy)=>{
                        success = (source.id === b.id && source.role === 'B');
                    });
                });

            };

            c1.setRole(a);
            c2.setRole(b);

            return eventually(()=>success);
        });

        it ("receives responder as role proxy", function() {

        });
    });

    describe('Roles', function() {
        it ('can listen for roles being created',function() {
            const b = new Role('B');
            const a = new Role('A');
            a.onWelcome = ()=>{ a.subscribe(b.name) };

            const c = new Client({url:url, domain:this.test.title, roles:[a] });

            c.onWelcome = ()=>{
                new Client({url:url, domain:this.test.title, roles:[b]});
            };

            return eventually(()=>a.getProxies(b.name).length > 0);
        });

        it('can listen for roles being destroyed',function() {
            let disconnected:Role[] = [];

            const a = new Role('A');
            const b = new Role('B');
            const c = new Role('C');
            const d = new Role('D');
            const e = new Role('E');
            const f = new Role('F');
            const g = new Role('G');
            const subscribedRoles = [b, c, d, e, f, g];

            a.onWelcome = ()=>{
                subscribedRoles.forEach(role=>{
                    a.subscribe(role.name, ()=>{}, r=>{disconnected.push(role)});
                });
            };

            const client = new Client({url:url, domain:this.test.title, roles:[a] });

            client.onWelcome = ()=>{
                const temp = new Client({url:url, domain:this.test.title, roles:subscribedRoles});
                temp.onWelcome = () => {
                    temp.closeConnection()
                }
            };

            return eventually(() => {
                let success = true;
                subscribedRoles.forEach(r=>{
                    if (!disconnected.includes(r)) success = false;
                });
                return success;
            });
        });

        it('can unsubscribe to role',function() {
            let timesCalled = 0;
            const b = new Role('B');
            const a = new Role('A');
            a.onWelcome = ()=>{ a.subscribe(b.name, ()=>{
                timesCalled++;
                a.unsubscribe(b.name);
            }) };

            const c = new Client({url:url, domain:this.test.title, roles:[a] });
            c.logger = nullLogger;

            c.onWelcome = ()=>{
                new Client({url:url, domain:this.test.title, roles:[b]});
                new Client({url:url, domain:this.test.title, roles:[b]});
            };

            return never(()=>timesCalled > 1);
        });

        it('will listen to all roles associated with a client', function() {
            const listener = new Client({url:url, domain:this.test.title});
            const role = new Role('Listener');
            listener.setRole(role);

            role.onWelcome = ()=> {
                role.subscribe('A', a => { notifiedAboutA = true; });
                role.subscribe('B', b => { notifiedAboutB = true; });
            };

            let notifiedAboutA = false;
            let notifiedAboutB = false;

            new Client({url:url, domain:this.test.title, roles:[new Role('A'), new Role('B')]});
            return eventually(()=>{return notifiedAboutA && notifiedAboutB;});
        });

        it('can get existing role instances', function() {
            const listener = new Client({url:url, domain:this.test.title});
            const c1 = new Client({url:url, domain:this.test.title, roles:[new Role('A')]});
            let id = -1;
            const l = listener.setRole(new Role('Listener'));
            l.onWelcome = ()=> { l.subscribe('A', r=>{id = r.id;}) };

            return eventually(()=>{return id === c1.id;});
        });

        it('sends all existing clients with role when subscribing',function() {
            const c1 = new Client({url:url, domain:this.test.title, roles:[new Role('A')]});
            const c2 = new Client({url:url, domain:this.test.title, roles:[new Role('A')]});
            const listener = new Client({url:url, domain:this.test.title});
            let sentC1 = false;
            let sentC2 = false;

            const l = listener.setRole(new Role("Listener"));
            l.onWelcome = ()=> {
                l.subscribe('A', a => {
                    if (a.id === c1.id) {
                        sentC1 = true;
                    } else if (a.id === c2.id) {
                        sentC2 = true;
                    }
                });
            };

            return eventually(()=>{return sentC1 && sentC2;});
        });

        it('does not receive role updates about itself', function() {
            const c = new Client({url:url, domain:this.test.title});
            let failed = false;

            const role = new Role('X');
            role.onWelcome = () => {
                role.subscribe('X', ()=>{failed = true}, ()=>{failed = true});
            };

            c.onWelcome = ()=>{
                c.setRole(role);
            };
            return never(()=>failed);
        });

        it('can send requests through a role instance', function() {
            const playerRole = new Role('Player');
            playerRole.setRequestHandler('GetName', ()=>"Bob");
            const player = new Client({url:url, domain:this.test.title, roles:[playerRole]});

            const game = new Client({url:url, domain:this.test.title });
            const g = game.setRole(new Role('Game'));
            g.onWelcome = () => {
                g.subscribe('Player', p=>{
                    g.requestFrom(p, 'GetName', (name:string)=>{
                        requestData = name;
                    })
                });
            };

            let requestData = '';

            return eventually(()=>{return requestData === 'Bob'});
        });

        it('can send messages through a role instance', function() {
            const playerRole = new Role('Player');
            playerRole.setMessageHandler('Hello', ()=>{received=true});

            const player = new Client({url:url, domain:this.test.title, roles:[playerRole]});

            let received = false;

            const game = new Client({url:url, domain:this.test.title, roles:[new Role('Game')]});
            const g = game.getRole('Game');

            g.onWelcome = ()=> g.subscribe('Player', p=>{g.sendTo(p, 'Hello')});

            return eventually(()=>received);
        });

        it('caches roles', function() {
            const player = new Client({url:url, domain:this.test.title, roles:[new Role('Player')]});
            const game = new Client({url:url, domain:this.test.title, roles:[new Role('Game')]});
            const g = game.getRole('Game');
            g.onWelcome = ()=> g.subscribe('Player');

            return eventually(()=>{
                const clients = g.getProxies("Player");
                if (clients.length)
                    return clients[0].id === player.id;
                else
                    return false;
            });
        });

        it('can add and remove roles after creation', function() {
            const listener = new Client({url:url, domain:this.test.title});
            const client = new Client({url:url, domain:this.test.title});

            let created = false;
            let destroyed = false;

            const l = listener.setRole(new Role('Listener'));
            l.onWelcome = ()=>{
                l.subscribe('Greeter',
                    ()=>{ created = true; },
                    ()=>{ destroyed = true; }
                );
            };

            client.onWelcome = ()=> {
                client.setRole(new Role("Greeter"));
                client.clearRole("Greeter");
            };

            return eventually(()=> created && destroyed );
        });

        it('only fires callback once when listening for role', function() {
            const listener = new Client({url:url, domain:this.test.title});
            const client = new Client({url:url, domain:this.test.title});

            let numTimesCalled = 0;
            const l = listener.setRole(new Role('Listener'));
            l.onWelcome = () => {
                l.subscribe('Test', ()=>{numTimesCalled++});
            };

            client.onWelcome = ()=>{
                client.setRole(new Role('Test'));
            };

            return eventually(()=> numTimesCalled === 1 );
        });

    });

    describe('Heartbeats', function() {

        it ('can change the heartbeat interval', function() {
            const c = new Client({url:url, domain:this.test.title});
            c.heartbeatInterval = 1;
            expect(c.heartbeatInterval).to.equal(1);
            c.closeConnection();
        });

        it ('can change the heartbeat timeout', function() {
            const c = new Client({url:url, domain:this.test.title});
            c.heartbeatTimeout = 1;
            expect(c.heartbeatTimeout).to.equal(1);
            c.closeConnection();
        });

        it ('does not send heartbeats when closed', function() {
            const c = new Client({url:url, domain:this.test.title});
            c.updateInterval = 0;
            c.heartbeatInterval = 10;
            c.onWelcome = ()=>{c.closeConnection()};
            c.onHeartbeatSent = ()=>{expect.fail()};
            return wait(c.heartbeatInterval * 2);
        });

        it ('sends heartbeats when connected', function () {
            const c = new Client({url:url, domain:this.test.title});
            c.updateInterval = 0;
            c.heartbeatInterval = 10;
            let heartbeat = false;
            c.onHeartbeatSent = ()=>{heartbeat = true};
            return eventually(()=>heartbeat);
        });

        it ('estimated latency is updated when heartbeat is received', function() {
            const c = new Client({url:url, domain:this.test.title});
            c.updateInterval = 0;
            c.heartbeatInterval = 5;
            let success = false;
            c.onHeartbeatReceived = ()=>{ success = c.estimatedLatency >= 0; };
            return eventually(()=>success);
        });

    });

    describe('Reconnect', function() {

        it('attempts to reconnect when connection fails', function() {
            const c = new Client({url:'ws://not.a.valid.url', domain:this.test.title});
            c.reconnectDelay = 1;
            let attemptedReconnection = false;
            c.onReconnectAttempt = ()=>{attemptedReconnection = true};
            return eventually(()=>attemptedReconnection);
        });

        it ('honors reconnection enabled flag', function () {
            const c = new Client({url:url, domain:this.test.title});
            c.isAutoReconnectEnabled = false;
            c.onWelcome = ()=>{simulateError(c)};
            return never(()=>c.reconnectAttempts > 0);
        });

        it('attempts to reconnect when closed by error', function() {
            const c = new Client({url:url, domain:this.test.title});
            c.reconnectDelay = 1;
            c.onWelcome = ()=>{simulateError(c)};
            return eventually(()=>c.reconnectAttempts > 0);
        });

        it('does not attempt to reconnect when closed by user', function() {
            const c = new Client({url:url, domain:this.test.title});
            c.onWelcome = ()=>{ c.closeConnection() };
            return never(()=>c.reconnectAttempts > 0);
        });

        it('makes limited reconnection attempts', function() {
            const c = new Client({url:'ws://localhost', domain:this.test.title});
            c.reconnectDelay = 1;
            c.maxReconnectAttempts = 3;
            return never(()=>c.reconnectAttempts > c.maxReconnectAttempts);
        });

        it('increases reconnection delay after each failed attempt', function() {
            const c = new Client({url:'ws://localhost', domain:this.test.title});
            c.reconnectDelay = 15;

            let lastReconnect = -1;
            let lastDuration = -1;
            let didFail = false;
            c.onReconnectAttempt = ()=>{
                const now = Date.now();
                if (lastReconnect >= 0) {
                    const duration = now - lastReconnect;
                    if (duration < lastDuration) didFail = true;
                    lastDuration = duration;
                } else {
                    lastDuration = 0;
                }
                lastReconnect = now;
            };
            return never(()=>didFail);
        });

        it ('can reconnect manually', function() {
            const c = new Client({url:url, domain:this.test.title});
            c.isAutoReconnectEnabled = false;
            let success = false;
            c.onWelcome = ()=>{ c.reconnect(); };
            c.onReconnect = ()=> { success = true };
            return eventually(()=>success);
        });

        it ('can reconnect with different connection context', function() {
            const c = new Client({url:url, domain:this.test.title});
            c.isAutoReconnectEnabled = false;
            let success = false;
            c.onWelcome = ()=>{
                c.context = {shouldFail: true};
                c.reconnect();
                c.onClose = (expected, code)=>{
                    success = code === 4001;
                };
            };
            return eventually(()=>success);
        });

    });
});