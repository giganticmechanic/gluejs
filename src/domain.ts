import * as WebSocket from 'uws';
import {
    GlueEvent, EventType, MessageEvent,
    RequestEvent, ResponseEvent, HeartbeatEvent, RoleSubscribeEvent, RoleUnsubscribeEvent,
    openEnvelope, RoleChangedEvent, ConnectEvent
} from './glue_event'
import {forEachValue, removeElement} from './utility';
import {LogLevel} from "./log_level";
import {Router} from "./router";
import {isUndefined} from "util";

export class ClientData {

    private _id:number;
    get id():number { return this._id; }

    private _roles:Array<string>;
    get roles() { return this._roles; }

    private _socket:WebSocket;
    get socket():WebSocket { return this._socket; }

    private _roleSubscriptions:Array<string> = [];
    public get roleSubscriptions() { return this._roleSubscriptions; }

    constructor(socket:WebSocket, id:number) {
        this._socket = socket;
        this._id = id;
        this._roles = [];
    }
}

export class Domain {

    private _clients:{[id:number]:ClientData} = {};

    private _parent:Router;
    public get parent() { return this._parent; }

    private _name:string;
    get name() { return this._name; }

    private _shouldCloseOnEmpty: boolean;
    get shouldCloseOnEmpty() { return this._shouldCloseOnEmpty; }
    set shouldCloseOnEmpty(newValue: boolean) { this._shouldCloseOnEmpty = newValue; }

    constructor(domainName:string, parent:Router, logLevel:LogLevel=LogLevel.NONE, shouldCloseOnEmpty: boolean) {
        this._name = domainName;
        this._parent = parent;
        this._shouldCloseOnEmpty = shouldCloseOnEmpty;
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // ROLES
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private roleCreated(role:string, clientId:number) {
        const client = this._clients[clientId];
        if (client !== undefined) {
            client.roles.push(role);
            this.sendRoleChangedEvent(GlueEvent.createRoleChangedEvent(role, clientId, true));
        } else {
            this.log(LogLevel.ERROR, "Unknown client adding role: (client id)" + clientId + ", (role) " + role);
        }
    }

    private roleDestroyed(role:string, clientId:number) {
        const client = this._clients[clientId];
        if (client !== undefined) {
            const i = client.roles.indexOf(role);
            if (i != -1) {
                client.roles.splice(i, 1);
                this.sendRoleChangedEvent(GlueEvent.createRoleChangedEvent(role, clientId, false));
            } else {
                this.log(LogLevel.ERROR, "Client removing unknown role: (client id)" + clientId + ", (role) " + role);
            }
        } else {
            this.log(LogLevel.ERROR, "Unknown client removing role: (client id)" + clientId + ", (role) " + role);
        }
    }
    
    private sendRoleChangedEvent(evt:RoleChangedEvent) {
        const messageString = JSON.stringify(evt);
        forEachValue(this._clients, (client:ClientData)=>{
            if (client.id !== evt.roleOwnerId && client.roleSubscriptions.includes(evt.role)) {
                client.socket.send(messageString, this.handleSendError);
            }
        });
    }

    private handleRoleChanged(source:ClientData, e:RoleChangedEvent) {
        if (e.wasCreated) {
            this.roleCreated(e.role, source.id);
        } else {
            this.roleDestroyed(e.role, source.id);
        }
    }
    
    private handleRoleSubscribe(source:ClientData, evt:RoleSubscribeEvent) {
        const clientsWithRole:Array<number> = [];
        forEachValue(this._clients, (client:ClientData)=>{
            if (client.roles.includes(evt.role) && client.id !== source.id) {
                clientsWithRole.push(client.id);
            }
        });
        const response = GlueEvent.createRoleSubscriptionResponseEvent(evt.role, clientsWithRole);
        source.roleSubscriptions.push(evt.role);
        source.socket.send(JSON.stringify(response), this.handleSendError);
    }

    // noinspection JSMethodCanBeStatic
    private handleRoleUnsubscribe(source:ClientData, e:RoleUnsubscribeEvent) {
        removeElement(source.roleSubscriptions, e.role);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // LOGGING
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    private log(logLevel:LogLevel, message:string) {
        this.parent.log(logLevel, message);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // EVENT HANDLING
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    private handleEvent(event:GlueEvent, source:ClientData, targets:Array<number>) {
        try {
            switch(event.type) {
                case EventType.CONNECT:
                    this.transferDomain(source, (event as ConnectEvent));
                    break;
                case EventType.MESSAGE:
                    this.handleMessage(source, event as MessageEvent, targets);
                    break;
                case EventType.REQUEST:
                    this.handleRequest(this._clients[targets[0]], event as RequestEvent);
                    break;
                case EventType.RESPONSE:
                    this.handleResponse(this._clients[targets[0]], event as ResponseEvent);
                    break;
                case EventType.HEARTBEAT:
                    this.handleHeartbeat(source, event as HeartbeatEvent);
                    break;
                case EventType.ROLE_CHANGED:
                    this.handleRoleChanged(source, event as RoleChangedEvent);
                    break;
                case EventType.ROLE_SUBSCRIBE:
                    this.handleRoleSubscribe(source, event as RoleSubscribeEvent);
                    break;
                case EventType.ROLE_UNSUBSCRIBE:
                    this.handleRoleUnsubscribe(source, event as RoleUnsubscribeEvent);
                    break;
                default:
                    //noinspection ExceptionCaughtLocallyJS
                    throw new Error('Domains cannot handle event type: ' + event.type);
            }
        } catch(e) {
            this.log(LogLevel.ERROR, "Exception handling event: " + JSON.stringify(event) + "\nException:\n"+e);
        }
    };

    private handleMessage(source:ClientData, event:MessageEvent, targets:Array<number>) {
        const message = JSON.stringify(event);
        for (let index in targets) {
            const targetId = targets[index];
            if (targetId !== source.id) {
                const client:ClientData = this._clients[targetId];
                if (client) {
                    client.socket.send(message, this.handleSendError);
                }
            }
        }

        if (LogLevel.DEBUG >= this._parent.logLevel) {
            this.log(LogLevel.DEBUG, "Forwarding message: " + JSON.stringify(event) + " to targets: " + JSON.stringify(targets) );
        }
    }

    private handleRequest(target:ClientData, event:RequestEvent) {
        if (target === undefined) throw new Error("Nonexistent target for request:" + JSON.stringify(event));
        target.socket.send(JSON.stringify(event), this.handleSendError);
        if (LogLevel.DEBUG >= this._parent.logLevel) {
            this.log(LogLevel.DEBUG, `Forwarding request to ${target.id}: ${JSON.stringify(event)}`);
        }
    }

    private handleResponse(target:ClientData, event:ResponseEvent) {
        if (target === undefined) throw new Error("Nonexistent target for response:" + JSON.stringify(event));
        target.socket.send(JSON.stringify(event), this.handleSendError);
        if (LogLevel.DEBUG >= this._parent.logLevel) {
            this.log(LogLevel.DEBUG, `Forwarding response to ${target.id}: ${JSON.stringify(event)}`);
        }
    }

    private handleHeartbeat(source:ClientData, event:HeartbeatEvent) {
        source.socket.send(JSON.stringify(event), this.handleSendError);
        if (LogLevel.DEBUG >= this._parent.logLevel) {
            this.log(LogLevel.DEBUG, "Heartbeat from: " + source.id);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // SOCKET MANAGEMENT
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private handleSendError = (err:any)=>{
        if (!isUndefined(err)) {
            this.log(LogLevel.ERROR, 'Socket send error: ' + JSON.stringify(err));
        }
    };

    private handleClose(socket:WebSocket) {
        // TODO: Keep a map of the sockets to some id so we can retrieve these more easily
        forEachValue(this._clients, (client:ClientData)=>{
            if (client.socket === socket) {
                for (let i = client.roles.length - 1; i >= 0; i--) {
                    this.roleDestroyed(client.roles[i], client.id);
                }
                delete (this._clients[client.id]);
                return;
            }
        });
    };

    private onError(client:ClientData, error:any) {
        this.log(LogLevel.ERROR, 'Socket Error (client id: '+client.id+'): ' + JSON.stringify(error));
    };

    private onMessage(source:ClientData, message:{data:string}) {
        try {
            const e = openEnvelope(message.data);
            this.handleEvent(e.event, source, e.targets);
        } catch (exception) {
            this.log(LogLevel.ERROR, 'Exception at socket.onmessage: (MSG: ' + JSON.stringify(message) + '), (EXC: ' + exception.toString() + ')');
        }
    };

    private onClose(client:ClientData) {
        try {
            this.handleClose(client.socket);
            this.log(LogLevel.INFO, "Client disconnected from "+this.name+": " + client.id);

            if (this._shouldCloseOnEmpty && Object.keys(this._clients).length === 0) {
                this.parent.closeDomain(this.name);
            }
        } catch (exception) {
            this.log(LogLevel.ERROR, 'Exception at onClose: ' + exception.toString());
        }
    };

    public connectClient(socket:WebSocket, id:number, roles:Array<string>) {
        if (this._clients[id] !== undefined) {
            socket.close(4003, "Connecting with duplicate id");
            this.log(LogLevel.WARNING, "Client attempting to connect with duplicate id: " + id);
            return;
        }
        const client = new ClientData(socket, id);
        this._clients[client.id] = client;

        roles.forEach((role)=>{
            this.roleCreated(role, client.id);
        });

        client.socket.onmessage = (message:{data:any})=>{
            this.onMessage(client, message);
        };

        client.socket.onerror = (err:any)=>{
            this.onError(client, err);
        };

        client.socket.onclose = ()=>{
            this.onClose(client);
        };

        const welcomeEvent = GlueEvent.createWelcome(this.name);
        client.socket.send(JSON.stringify(welcomeEvent), this.handleSendError);

        if (LogLevel.INFO > this._parent.logLevel) {
            this.log(LogLevel.INFO, "Client connected to "+this.name+". Roles: " + JSON.stringify(client.roles) + ", ID: " + client.id);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MISC
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    close() {
        for (let id in this._clients) {
            const client = this._clients[id];
            client.socket.close(1013, 'Try again later');
        }
    }

    killClient(clientId:number) {
        const client = this._clients[clientId];
        if (client) {
            client.socket.close(1013, 'Try again later');
        }
    }

    hasClient(id:number) {
        return this._clients[id] !== undefined;
    }

    private transferDomain(client:ClientData, event:ConnectEvent) {
        if (event.domain !== this.name) {
            this._parent.transferToDomain(client.socket, event);
            this.onClose(client);
        }
    }
}