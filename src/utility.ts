
const IdUpperBound = (Math.pow(2, 53) - 1);
export function createId() {
    let id = 0;
    while (id === 0) {
        id = Math.floor(Math.random() * IdUpperBound);
    }
    return id;
}

export function removeAtIndex(list:Array<any>, index:number) {
    list.splice(index, 1);
}

export function removeElement(list:Array<any>, element:any) {
    if (list === undefined) return;
    const i = list.indexOf(element);
    if (i !== -1) {
        list.splice(i, 1);
    }
}

export function removeFirstMatchingElement(list:Array<any>, pred:(el:any)=>boolean):any {
    if (list === undefined) return undefined;
    for (let i = 0; i < list.length; i++) {
        const el = list[i];
        if (pred(el)) {
            list.splice(i, 1);
            return el;
        }
    }
    return undefined;
}

export function forEachValue(obj:{}, fn:(val:any)=>void) {
    const keys = Object.keys(obj);
    const l=keys.length;
    for(let i=0; i<l; i++) {
        fn(obj[keys[i]]);
    }
}
