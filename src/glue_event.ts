import {RequestError} from "./request_error";

// TODO: Make target lists a feature of envelopes (most of the time you send individual ones and requests/responses never target multiples)
// TODO: Might be able to collapse the message, request and response events into one
// TODO: Collapse role subscribe/unsubscribe into role_subscriptionChange
export enum EventType {
    CONNECT = 0,
    WELCOME = 1,
    MESSAGE = 2,
    REQUEST = 3,
    RESPONSE = 4,
    HEARTBEAT = 5,
    ROLE_CHANGED = 6,
    ROLE_SUBSCRIBE = 7,
    ROLE_UNSUBSCRIBE = 8,
    ROLE_SUBSCRIPTION_RESPONSE = 9,
}

export function createEnvelope(evt:GlueEvent, targets:Array<number> = [] ) {
    const o = {event:evt, targets:targets};
    return JSON.stringify(o);
}

export function openEnvelope(json:string): {event:GlueEvent, targets:Array<number>} {
    const o = JSON.parse(json);
    return {event:o.event, targets:o.targets};
}

export interface GlueEvent {type:EventType}

export interface MessageEvent extends GlueEvent {name:string, targetRole:string, sourceRole:string, source:number, data:any|null}
export interface RequestEvent extends GlueEvent {name:string, targetRole:string, sourceRole:string, source:number, requestId:number, data:any|null}
export interface ResponseEvent extends GlueEvent {name:string, targetRole:string, sourceRole:string, source:number, requestId:number, data:any|null, error:RequestError|null}

export interface ConnectEvent extends GlueEvent {domain:string, id:number, roles:Array<string>, context:any}
export interface WelcomeEvent extends GlueEvent {domain:string}
export interface HeartbeatEvent extends GlueEvent {timestamp:number}

export interface RoleChangedEvent extends GlueEvent {role:string, roleOwnerId:number, wasCreated:boolean}
export interface RoleSubscribeEvent extends GlueEvent {role:string, subscriberId:number}
export interface RoleSubscriptionResponseEvent extends GlueEvent {role:string, roleOwners:Array<number>}
export interface RoleUnsubscribeEvent extends GlueEvent {role:string, subscriberId:number}

export class GlueEvent implements GlueEvent{

    static createConnect(domain:string, id:number, roles:Array<string>, context?:any):ConnectEvent {
        return {type:EventType.CONNECT, domain:domain, id:id, roles:roles, context:context};
    }

    static createMessage(name:string, targetRole:string, sourceRole:string, source: number, data:any):MessageEvent {
        return {type:EventType.MESSAGE, targetRole:targetRole, source: source, sourceRole: sourceRole, name:name, data:data};
    }

    static createRequest(name:string, targetRole:string, sourceRole: string, source:number, requestId:number, data:any):RequestEvent {
        return {type:EventType.REQUEST, name:name, targetRole:targetRole, data:data, sourceRole:sourceRole, source:source, requestId:requestId};
    }

    static createResponse(name:string, targetRole:string, sourceRole: string, source:number, requestId:number, data:any, error:RequestError|null):ResponseEvent {
        return {type:EventType.RESPONSE, name:name, targetRole:targetRole, source: source, sourceRole:sourceRole, data:data, requestId:requestId, error:error};
    }

    static createWelcome(domain:string):WelcomeEvent {
        return {type:EventType.WELCOME, domain:domain};
    }

    static createHeartbeat(timestamp:number):HeartbeatEvent {
        return {type:EventType.HEARTBEAT, timestamp:timestamp};
    }

    static createRoleChangedEvent(role:string, roleOwnerId:number, wasCreated:boolean):RoleChangedEvent {
        return { type:EventType.ROLE_CHANGED, role:role, roleOwnerId:roleOwnerId, wasCreated:wasCreated};
    }

    static createRoleSubscribeEvent(role:string, clientId:number):RoleSubscribeEvent {
        return { type:EventType.ROLE_SUBSCRIBE, role:role, subscriberId:clientId};
    }

    static createRoleUnsubscribeEvent(role:string, clientId:number):RoleUnsubscribeEvent {
        return { type:EventType.ROLE_UNSUBSCRIBE, role:role, subscriberId:clientId};
    }

    static createRoleSubscriptionResponseEvent(role:string, roleOwners:Array<number>):RoleSubscriptionResponseEvent {
        return { type:EventType.ROLE_SUBSCRIPTION_RESPONSE, role:role, roleOwners:roleOwners };
    }
}