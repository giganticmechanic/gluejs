import {Server} from 'uws';
import * as WebSocket from 'uws';
import {EventType, ConnectEvent, openEnvelope} from './glue_event'
import {LogLevel} from "./log_level";
import {Domain} from "./domain";
import {IncomingHttpHeaders} from "http";
import {isUndefined} from "util";

const noop = ()=>{};

interface PendingConnection {socket:WebSocket, connectionTime:number}

export class Router {

    protected _server:Server;
    get server():Server { return this._server; }

    private _lobby:Array<PendingConnection> = [];

    private _domains:{[name:string]:Domain} = {};

    private _onDomainCreated:(domainName:string)=>void = noop;
    public set onDomainCreated(handler:(domainName:string)=>void) { this._onDomainCreated = handler; }

    private _onDomainDestroyed:(domainName:string)=>void = noop;
    public set onDomainDestroyed(handler:(domainName:string)=>void) { this._onDomainDestroyed = handler; }

    private _onServerCreated:()=>void = noop;
    public set onServerCreated(handler:()=>void) { this._onServerCreated = handler; }

    private _newConnectionHandler:(connectEvent:ConnectEvent, headers:IncomingHttpHeaders|null)=>{success:boolean, reason?:string} = ()=>{ return {success:true} };
    public set onHandleNewConnection(handler:(evt:ConnectEvent, headers:IncomingHttpHeaders|null)=>{success:boolean, reason?:string}) { this._newConnectionHandler = handler; }

    private _connectionTimeout:number;
    get connectionTimeout() { return this._connectionTimeout };
    set connectionTimeout(interval:number) {
        this._connectionTimeout = interval;
        if (this._connectionTimer) {clearInterval(this._connectionTimer)}
        this._connectionTimer = setInterval(()=>{this.clearStaleConnections()}, this._connectionTimeout);
    };

    private _connectionTimer:any;

    constructor(options:WebSocket.IServerOptions, logLevel:LogLevel = LogLevel.NONE) {

        this.connectionTimeout = 5000;
        this.logLevel = logLevel;

        options.handleProtocols = (protocols:Array<string>)=>{
            for (let i = 0; i < protocols.length; i++) {
                if (protocols[i] === 'glue') return true;
            }
            return false;
        };

        this._server = new Server(options, (err:any)=>{
            if (err) {
                throw new Error('Could not create ws server: ' + JSON.stringify(err));
            } else {
                this._onServerCreated();
            }
        });

        this.server.on('error', (err:Error)=> {
            throw err;
        });

        this.server.on('connection', (socket:WebSocket)=>{
            const headers:IncomingHttpHeaders = socket.upgradeReq.headers;
            this._lobby.push({socket: socket, connectionTime: Date.now()});

            if (this.logLevel <= LogLevel.INFO) {
                const origin:string = (headers instanceof Array) ? headers[0]['origin']: headers['origin'];
                this.log(LogLevel.INFO, "Socket connecting from " + origin);
            }

            socket.onerror = (error:any)=>{
                const connection = this.getConnection(socket) as PendingConnection;
                this.clearConnection(connection);
                this.log(LogLevel.ERROR, "Router socket error: " + error.toString());
            };

            socket.onmessage = (message:{data:any})=> {
                try {
                    const e = openEnvelope(message.data);
                    if (e.event.type !== EventType.CONNECT) {
                        throw new Error("New client sent message other than CONNECT: " + e.event.type);
                    }
                    const event = e.event as ConnectEvent;
                    this.onConnect(socket, event, headers);
                } catch (exc) {
                    socket.close(1011, "Could not connect due to router error.");
                    this.log(LogLevel.ERROR, "Message: "+ JSON.stringify(message.data) + ", EXCEPTION: " + exc.toString());
                }
            };

            socket.onclose = (event)=>{
                this.log(LogLevel.INFO, `Closing socket: code: ${event.code} reason: ${event.reason}`);
                const connection = this.getConnection(socket);
                if (connection) {
                    this.clearConnection(connection);
                }
            };
        });

        // Handle shut down
        process.on('exit', ()=>{
            this.shutDown();
        });

        // catch ctrl+c event and exit normally
        process.on('SIGINT', ()=> {
            this.log(LogLevel.WARNING, 'Router received SIGINT');
            process.exit(2);
        });

        //catch uncaught exceptions, trace, then exit normally
        process.on('uncaughtException', (err)=>{
            this.log(LogLevel.ERROR, 'Router had unhandled exception: ' + err.message);
            process.exit(99);
        });

    }

    private onConnect(socket:WebSocket, event:ConnectEvent, headers:IncomingHttpHeaders|null) {
        const validation = this._newConnectionHandler(event, headers);
        if (isUndefined(validation) || isUndefined(validation.success)) {
            throw new Error("onHandleNewConnection has to return an object that has a 'success' property (e.g. {success:false})");
        }

        if (!validation.success) {
            socket.close(4001, "Failed validation: " + validation.reason);
        } else {
            const domain = this._domains[event.domain];
            if (!domain) {
                socket.close(4002, "Missing domain: " + event.domain);
            } else {
                domain.connectClient(socket, event.id, event.roles);
                const connection = this.getConnection(socket);
                if (connection) this.clearConnection(connection);
            }
        }
    }

    shutDown() {
        Object.keys(this._domains).forEach((key)=> {
            const domain = this._domains[key];
            domain.close();
        });
        this._server.close();
    }

    private getConnection(socket:WebSocket):PendingConnection|null {
        for (let i = 0; i < this._lobby.length; i++) {
            const connection = this._lobby[i];
            if (connection.socket === socket) return connection;
        }
        return null;
    }

    private clearConnection(connection:PendingConnection) {
        for (let i = 0; i < this._lobby.length; i++) {
            if (this._lobby[i] === connection) {
                this._lobby.splice(i, 1);
                break;
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // DOMAINS
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    createDomain(domainName:string, shouldCloseOnEmpty: boolean) {
        let domain = this._domains[domainName];
        if (isUndefined(domain)) {
            this._domains[domainName] = domain = new Domain(domainName, this, this.logLevel, shouldCloseOnEmpty);
            this._onDomainCreated(domainName);
            this.log(LogLevel.INFO, "Created domain: " + domainName);
        }
        return domain;
    }

    closeDomain(domainName:string) {
        const domain = this._domains[domainName];
        if (domain !== undefined) {
            domain.close();
            delete this._domains[domainName];
            this.log(LogLevel.INFO, "Destroyed domain: " + domainName);
            this._onDomainDestroyed(domainName);
        }
    }

    getActiveDomains():Array<string> {
        return Object.keys(this._domains);
    }

    getDomain(name:string):Domain|null {
        const domain = this._domains[name];
        return domain? domain:null;
    }

    /** @internal **/
    transferToDomain(socket:WebSocket, event:ConnectEvent) {
        this.onConnect(socket, event, null);
    }

    private clearStaleConnections() {
        const now = Date.now();
        for (let i = this._lobby.length -1; i > 0; i--) {
            const connection = this._lobby[i];
            if ((now - connection.connectionTime) > this._connectionTimeout - 2) {
                connection.socket.close(4004, "Timed out waiting for Connect event");
                this.clearConnection(connection);
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // LOGGING
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private _logHandler = (logLevel:LogLevel, message:string)=> {
        const time = new Date().toISOString().substr(11).replace('Z', "");
        const msg = "[" + LogLevel[logLevel] + ": " + time + "]"  + ": " + message;
        switch (logLevel) {
            case LogLevel.DEBUG:
            case LogLevel.INFO:
                console.log(msg);
                break;
            case LogLevel.WARNING:
            case LogLevel.ERROR:
                console.error(msg);
                break;
        }
    };

    setLogHandler(handler:(logLevel:LogLevel, message:string)=>void) {
        this._logHandler = handler;
    }

    public log(level:LogLevel, message:string) {
        if (level >= this.logLevel) {
            this._logHandler(level, message);
        }
    }

    private _logLevel:LogLevel;
    set logLevel(logLevel:LogLevel) {
        this._logLevel = logLevel;
    }

    get logLevel() {
        return this._logLevel;
    }
}
