import {RequestEvent} from "./glue_event";

export enum ErrorType {
    REQUEST_TIMEOUT,
    UNHANDLED_REQUEST,
    REQUEST_FAILED
}

export class RequestError {

    type:ErrorType;
    reason:string;

    constructor(type:ErrorType, reason:string) {
        this.type = type;
        this.reason = reason;
    }

    static createRequestTimeoutError() {
        return new RequestError(ErrorType.REQUEST_TIMEOUT, "Request timed out");
    };

    static createRequestFailedError() {
        return new RequestError(ErrorType.REQUEST_FAILED, "Request could not be completed due to error at callee");
    };

    static createUnhandledRequestError(unhandledRequest:RequestEvent) {
        return new RequestError(ErrorType.UNHANDLED_REQUEST, "Client has no handler for request: " + unhandledRequest.name);
    };

}