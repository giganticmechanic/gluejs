"use strict";

const glue = require('./dist');

let logLevel;
let port = 5000;

process.argv.forEach((arg)=>{
    if (typeof (arg) === 'number') {
        port = arg;
    } else if (typeof (arg) === 'string') {
        logLevel = glue.LogLevel[arg];
    }
});

if (logLevel === undefined) logLevel = glue.LogLevel.NONE;

console.log(`Creating router at port ${port} with log level ${glue.LogLevel[logLevel]}`);
const router = new glue.Router({port:port}, logLevel);

router.onHandleNewConnection = (connectEvent/*, headers */)=>{
    const context = connectEvent.context;
    let shouldCreateDomain = true;
    if (context) {
        if (context.shouldFail) {
            return {success:false, reason: "`shouldFail` was set to `true`"};
        }
        if (context.shouldNotCreateDomain) shouldCreateDomain = false;
    }
    if (shouldCreateDomain) {
        router.createDomain(connectEvent.domain, true);
    }
    return {success:true}
};