const browserConfig = function (env) {
    return {
        entry: "./dist/client.js",
        target: "web",
        output: {
            libraryTarget: "umd",
            library: "glue",
            umdNamedDefine: true,
            filename: "./dist/glue" + ( env.production ? ".min.js" : ".js")
        },
        resolve: {
            extensions: [".js"],
            alias: {
                './websocket':'./browser_websocket'
            }
        },
    }
};

module.exports = function (env) {
    if (!env) env = {};
    return [browserConfig(env)];
};